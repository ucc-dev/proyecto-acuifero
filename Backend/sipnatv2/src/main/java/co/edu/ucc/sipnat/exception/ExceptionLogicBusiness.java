package co.edu.ucc.sipnat.exception;

import org.springframework.http.HttpStatus;

public class ExceptionLogicBusiness extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final HttpStatus httpStatus;

	public ExceptionLogicBusiness(String mensaje, HttpStatus status) {
		super(mensaje);
		this.httpStatus = status;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

}
