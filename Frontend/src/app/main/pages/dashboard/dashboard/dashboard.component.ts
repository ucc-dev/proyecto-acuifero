import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { Sensor } from 'app/_model/Sensor';
import { AuthenticationService } from 'app/_services/authentication/authentication.service';
import { DashboardService } from 'app/_services/dashboard/dashboard.service';
import { SENSOR_STATE_CONNECTED, SENSOR_STATE_CREATED, SENSOR_STATE_DISCONNECTED } from 'app/_utils/constants';
import { ChartDataSets, ChartOptions, ChartPoint, ChartType, TimeUnit } from 'chart.js';
import { environment } from 'environments/environment';
import * as L from "leaflet";
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: fuseAnimations
})
export class DashboardComponent implements OnInit {

  //Elements for the graph
  projects: any[];
  selectedProject: any;
  numberSensorsStateCreated: number;
  numberSensorsStateConnected: number;
  numberSensorsStateDisconnected: number;

  widget5SelectedDay = "TODAY";
  ready: boolean = false;
  chartType: ChartType = "line";
  options: ChartOptions;
  dataSet: ChartDataSets[];

  //Elements for the Map
  map: L.Map;

  optionsMap: L.MapOptions = {
    layers: [
      L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        minZoom: 0,
        maxZoom: 19,
        attribution: "",
      }),
    ],
    zoom: 14,
    center: L.latLng(0, 0),
    maxZoom: 19,
    zoomAnimation: true,
  };

  constructor(
    private _fuseSidebarService: FuseSidebarService,
    private dashboardService: DashboardService,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private router: Router) { }

  ngOnInit(): void {
    moment.locale('es');

    this.getProjects();
    this.getIdParam();

    this.numberSensorsStateCreated = 0;
    this.numberSensorsStateConnected = 0;
    this.numberSensorsStateDisconnected = 0;
  }

  toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }

  getIdParam() {
    this.activatedRoute.params.subscribe(resp => {
      let id = resp['id'];
      if (id != null && id != undefined) {
        this.ready = false;
        this.inicializateProject(id);
      }
    });
  }

  changeDashboard(project: any) {
    this.ready = false;
    this.router.navigate([`pages/dashboard/${project.id}`])
  }

  getProjects() {
    this.dashboardService.getProjects().subscribe(resp => {
      this.projects = resp;
    })
  }

  inicializateProject(id: number) {
    this.dashboardService.getProject(id).subscribe(resp => {
      this.selectedProject = resp;
      this.getDataSensorsToGraphic(resp.id);

      this.inicializateWidgets(resp.id, SENSOR_STATE_CONNECTED);
      this.inicializateWidgets(resp.id, SENSOR_STATE_DISCONNECTED);
      this.inicializateWidgets(resp.id, SENSOR_STATE_CREATED);
    });
  }

  inicializateWidgets(id: number, state: string) {
    this.dashboardService.countSensorsByState(id, state).subscribe(resp => {
      switch (state) {
        case SENSOR_STATE_CREATED:
          this.numberSensorsStateCreated = resp;
          break;
        case SENSOR_STATE_CONNECTED:
          this.numberSensorsStateConnected = resp;
          break;
        case SENSOR_STATE_DISCONNECTED:
          this.numberSensorsStateDisconnected = resp;
          break;
        default:
          break;
      }
    });
  }

  getDataSensorsToGraphic(id: number) {
    this.dashboardService.getDataSensors(id, this.widget5SelectedDay).subscribe(resp => {

      this.dataSet = [];

      for (let sensor of resp) {
        let dataForDataset: ChartPoint[] = new Array();

        for (let temp of sensor.listData) {
          dataForDataset.push({
            x: temp.fechaRecolecion,
            y: temp.dato,
          });
        }

        this.dataSet.push({
          data: dataForDataset,
          label: `${sensor.sensorName} - ${sensor.unitMeasure}`,
          fill: false
        });
      }

      this.inicializateGraph("Unidad de medida", this.widget5SelectedDay == "ALL" ? "day" : "hour");
      this.ready = true;
    });
  }

  inicializateGraph(
    labelY: string,
    timeUnit: TimeUnit
  ) {
    this.options = {
      responsive: true,
      spanGaps: false,
      maintainAspectRatio: false,
      tooltips: {
        position: "nearest",
        mode: "point",
        intersect: false,
      },
      layout: {
        padding: {
          left: 24,
          right: 32,
        },
      },
      elements: {
        point: {
          radius: 4,
          borderWidth: 2,
          hoverRadius: 4,
          hoverBorderWidth: 2,
        },
      },
      scales: {
        xAxes: [
          {
            type: "time",
            distribution: "linear",
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            },
            time: {
              unit: timeUnit,
              displayFormats: {
                minute: "HH:mm",
              },
            },
            scaleLabel: {
              display: true,
              labelString: "Fecha Recolección",
            },
          },
        ],
        yAxes: [
          {
            display: true,
            scaleLabel: {
              display: true,
              labelString: labelY,
            },
            gridLines: {
              display: true,
              drawBorder: true,
            },
          },
        ],
      },
      plugins: {
        zoom: {
          pan: {
            enabled: true,
            mode: "xy",
          },
          zoom: {
            enabled: true,
            mode: "xy",
          },
        },
      },
    };
  }

  onMapReady(event: any) {
    this.map = event;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        this.setGeoLocation.bind(this)
      );
    }

    this.addMarkersToMap(this.selectedProject);
  }

  setGeoLocation(position) {
    this.map.panTo(
      new L.LatLng(position.coords.latitude, position.coords.longitude)
    );
  }

  addMarkersToMap(selectedProject: any) {
    this.dashboardService.getSensors(selectedProject.id).subscribe(resp => {
      for (let sensor of resp) {
        let icon = L.icon({
          iconUrl: `${environment.host}/sensortype/icon/${sensor.idSensorType}`,
          iconSize: [48, 48],
          popupAnchor: [0, -24],
        });

        let options: L.MarkerOptions = {
          icon: icon,
        };

        let latitude = parseFloat(sensor.latitude);
        let longitude = parseFloat(sensor.longitude);

        L.marker(new L.LatLng(latitude, longitude), options)
          .addTo(this.map)
          .bindPopup(`<b>Posición del sensor: </b> ${sensor.name} 
                      <br/> 
                      <b>Estado: </b> ${sensor.status}
                      <br/> 
                      <b>Tipo: </b> ${sensor.sensorTypeName}
                      <br/> 
                      <b>Unidad de medida: </b> ${sensor.unitMeasure}
                      <br/> 
                      <b>Magnitud: </b> ${sensor.magnitude}
                      <br/> 
                      <b>Ultima comunicación: </b> ${this.formatDateFromNow(sensor.dateLastData)}`);

        this.map.panTo(new L.LatLng(latitude, longitude));
      }
    });
  }

  formatDateFromNow(date) {
    if (date == undefined || date == null) {
      return '';
    }

    return moment(date).fromNow()
  }

  infoUser(): any {
    return this.authenticationService.getAccessTokenData();
  }

  nameUser(): string {
    let name: string = this.infoUser().given_name;
    return name.charAt(0).toUpperCase() + name.slice(1);
  }
}


