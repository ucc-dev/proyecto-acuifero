export class Sensor {
    id: number;
    protocol: string;
    user: string;
    password: string;
    idSensorType: number;
    name: string;
    minLimit: number;
    maxLimit: number;
    latitude: string;
    longitude: string;
    status: string;
    sensorTypeName: string;
    unitMeasure: string;
    magnitude: string;
    dateLastData: string
    topic: string

    constructor() { }
}
