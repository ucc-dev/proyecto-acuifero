import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SensorType } from "app/_model/SensorType";
import { environment } from "environments/environment";

@Injectable({
    providedIn: "root",
})
export class SensorTypeService {
    private endpoint: string;

    constructor(private http: HttpClient) {
        this.endpoint = `${environment.host}/sensortype`;
    }

    createSensorType(sensorType: SensorType) {
        let formData: FormData = new FormData();
        formData.append("name", sensorType.name);
        formData.append("magnitude", sensorType.magnitude);
        formData.append("unitMeasure", sensorType.unitMeasure);
        formData.append("icon", sensorType.icon);
        return this.http.post<any>(this.endpoint, formData, {
            reportProgress: true,
            responseType: "json",
        });
    }

    listSensorType() {
        return this.http.get<SensorType[]>(this.endpoint);
    }

}
