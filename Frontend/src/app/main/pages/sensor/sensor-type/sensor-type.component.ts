import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ThemePalette } from "@angular/material/core";
import { SensorType } from "app/_model/SensorType";
import { SensorTypeService } from "app/_services/sensorType/sensor-type.service";
import swal from "sweetalert2";

@Component({
    selector: "app-sensor-type",
    templateUrl: "./sensor-type.component.html",
    styleUrls: ["./sensor-type.component.scss"],
})
export class SensorTypeComponent implements OnInit {
    form: FormGroup;

    color: ThemePalette = "primary";
    multiple: boolean = false;
    accept: string = ".png, .jpg, .jpeg";

    constructor(
        private formBuilder: FormBuilder,
        private sensorTypeService: SensorTypeService
    ) { }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: ["", Validators.required],
            magnitude: ["", Validators.required],
            unit: ["", Validators.required],
            icon: ["", Validators.required],
        });
    }

    get controls() {
        return this.form.controls;
    }

    onSubmit() {
        if (this.form.valid) {
            swal.fire({
                title: "Creando...",
                allowOutsideClick: false,
                allowEscapeKey: false,
            });

            swal.showLoading();

            let sensorType: SensorType = new SensorType();
            sensorType.name = this.controls.name.value;
            sensorType.magnitude = this.controls.magnitude.value;
            sensorType.unitMeasure = this.controls.unit.value;
            sensorType.icon = this.controls.icon.value;

            this.sensorTypeService.createSensorType(sensorType).subscribe(resp => {
                console.log(resp);
                swal.close();
                swal.fire({
                    title: 'Creado con éxito.',
                    showDenyButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: 'Ok',
                    icon: 'success'
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.clean();
                    }
                });
            }, error => {
                console.log(error);
                swal.close();
                swal.fire(
                    "ERROR",
                    "Se ha presentado un error, intente nuevamente",
                    "error"
                );
            });
        }
    }

    clean() {
        this.ngOnInit();
    }
}
