import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMapSensorComponent } from './modal-map-sensor.component';

describe('ModalMapSensorComponent', () => {
  let component: ModalMapSensorComponent;
  let fixture: ComponentFixture<ModalMapSensorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalMapSensorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMapSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
