package co.edu.ucc.sipnat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.SensorTablero;

@Repository
public interface ISensorDashboardDAO extends JpaRepository<SensorTablero, Long> {
	
	@Query(value = "select st from SensorTablero st where st.tablero.id=:idTablero")
	List<SensorTablero> findAllByIdTablero(@Param("idTablero") Long idTablero);

}
