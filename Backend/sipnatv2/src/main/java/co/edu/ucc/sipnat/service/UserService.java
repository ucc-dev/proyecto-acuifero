package co.edu.ucc.sipnat.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.authorization.client.util.HttpResponseException;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import co.edu.ucc.sipnat.dto.LogoutDTO;
import co.edu.ucc.sipnat.dto.RefreshTokenDTO;
import co.edu.ucc.sipnat.dto.UserDTO;
import co.edu.ucc.sipnat.exception.ExceptionLogicBusiness;
import co.edu.ucc.sipnat.exception.ExceptionUserCreation;
import co.edu.ucc.sipnat.util.Constants;

@Service
public class UserService {

	private Keycloak keycloak;

	private Logger logger = LogManager.getLogger(UserService.class);

	@Value("${keycloak.auth-server-url}")
	private String authServerUrl;

	@Value("${keycloak.realm}")
	private String keycloakRealm;

	@Value("${keycloak.resource}")
	private String keycloakClientId;

	@Value("${keycloak_role}")
	private String keycloakRole;

	@Value("${keycloak_client_secret}")
	private String keycloakClientSecret;

	@Value("${keycloak_user_admin}")
	private String keycloakUserAdmin;

	@Value("${keycloak_user_password}")
	private String keycloakUserPassword;

	@PostConstruct
	public void init() {
		keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl).realm(keycloakRealm)
				.grantType(OAuth2Constants.PASSWORD).clientId(Constants.KEYCLOAK_ADMIN_CLI).username(keycloakUserAdmin)
				.password(keycloakUserPassword)
				.resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();
	}

	public UserRepresentation createUser(UserDTO userDTO) {
		Response response = null;
		try {
			if (keycloak.isClosed()) {
				logger.info("createUser - keycloak is closed");
				init();
			}

			UserRepresentation user = new UserRepresentation();
			user.setEnabled(true);
			user.setUsername(userDTO.getEmail());
			user.setFirstName(userDTO.getFirstname());
			user.setLastName(userDTO.getLastname());
			user.setEmail(userDTO.getEmail());

			Map<String, List<String>> attributes = new HashMap<>();
			attributes.put(Constants.KEYCLOAK_ATTRIBUTE_PHONENUMBER, Arrays.asList(userDTO.getPhoneNumber()));

			user.setAttributes(attributes);

			// Get realm
			RealmResource realmResource = keycloak.realm(keycloakRealm);
			UsersResource usersRessource = realmResource.users();

			response = usersRessource.create(user);

			userDTO.setStatusCode(response.getStatus());
			userDTO.setStatus(response.getStatusInfo().toString());

			if (response.getStatus() == 201) {

				String userId = CreatedResponseUtil.getCreatedId(response);

				// create password credential
				CredentialRepresentation passwordCred = new CredentialRepresentation();
				passwordCred.setTemporary(false);
				passwordCred.setType(CredentialRepresentation.PASSWORD);
				passwordCred.setValue(userDTO.getPassword());

				UserResource userResource = usersRessource.get(userId);

				// Set password credential
				userResource.resetPassword(passwordCred);

				// Get realm role student
				RoleRepresentation realmRoleUser = realmResource.roles().get(keycloakRole).toRepresentation();

				// Assign realm role student to user
				userResource.roles().realmLevel().add(Arrays.asList(realmRoleUser));

				logger.info("Created userId {}", userId);

				return user;
			} else {
				throw new ExceptionUserCreation(response.getStatusInfo().toString(),
						HttpStatus.valueOf(response.getStatus()));
			}
		} catch (Exception e) {
			logger.error("createUser", e);
			throw e;
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	public AccessTokenResponse signin(UserDTO userDTO) {
		try {

			Map<String, Object> clientCredentials = new HashMap<>();
			clientCredentials.put(Constants.SECRET, keycloakClientSecret);
			clientCredentials.put(OAuth2Constants.GRANT_TYPE, OAuth2Constants.PASSWORD);

			Configuration configuration = new Configuration(authServerUrl, keycloakRealm, keycloakClientId,
					clientCredentials, null);
			AuthzClient authzClient = AuthzClient.create(configuration);

			return authzClient.obtainAccessToken(userDTO.getEmail(), userDTO.getPassword());
		} catch (HttpResponseException e) {
			throw new ExceptionUserCreation(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
		}
	}

	public AccessTokenResponse refreshToken(RefreshTokenDTO refreshTokenDTO) {
		try {
			String url = authServerUrl + "/realms/" + keycloakRealm + "/protocol/openid-connect/token";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
			map.set(OAuth2Constants.CLIENT_ID, keycloakClientId);
			map.set(OAuth2Constants.CLIENT_SECRET, keycloakClientSecret);
			map.set(OAuth2Constants.GRANT_TYPE, OAuth2Constants.REFRESH_TOKEN);
			map.set(OAuth2Constants.REFRESH_TOKEN, refreshTokenDTO.getRefreshToken());

			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<AccessTokenResponse> resp = restTemplate.exchange(url, HttpMethod.POST, entity,
					AccessTokenResponse.class);

			if (resp.getStatusCode().equals(HttpStatus.OK)) {
				return resp.getBody();
			}

		} catch (Exception e) {
			throw new ExceptionUserCreation(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
		}
		return null;
	}

	public boolean logout(LogoutDTO logoutDTO) {
		try {
			String url = authServerUrl + "/realms/" + keycloakRealm + "/protocol/openid-connect/logout";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.set(Constants.AUTHORIZATION, Constants.AUTHORIZATION_BEARER + logoutDTO.getAccessToken());

			MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
			map.set(OAuth2Constants.CLIENT_ID, keycloakClientId);
			map.set(OAuth2Constants.CLIENT_SECRET, keycloakClientSecret);
			map.set(OAuth2Constants.REFRESH_TOKEN, logoutDTO.getRefreshToken());

			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Object> resp = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);

			if (resp.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
				return true;
			}

		} catch (Exception e) {
			throw new ExceptionUserCreation(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
		}
		return false;
	}

	public List<UserDTO> listUsers() {
		logger.info("inicializate listUsers");
		RealmResource realmResource = keycloak.realm(keycloakRealm);
		List<UserDTO> users = new ArrayList<>();
		UsersResource usersRessource = realmResource.users();

		for (UserRepresentation userRepresentation : usersRessource.list()) {
			try {
				if (userRepresentation.getEmail().isEmpty() || userRepresentation.getFirstName().isEmpty()
						|| userRepresentation.getLastName().isEmpty()) {
					logger.error("user with incomplete data {}", userRepresentation);
					throw new ExceptionLogicBusiness("User with incomplete data", HttpStatus.BAD_REQUEST);
				}

				UserDTO user = new UserDTO();

				user.setId(userRepresentation.getId());
				user.setFirstname(userRepresentation.getFirstName());
				user.setLastname(userRepresentation.getLastName());
				user.setEmail(userRepresentation.getEmail());
				user.setEnabled(userRepresentation.isEnabled());
				Map<String, List<String>> attributes = userRepresentation.getAttributes();

				if (attributes != null) {
					List<String> attributePhoneNumber = attributes.getOrDefault("phoneNumber", null);
					user.setPhoneNumber(attributePhoneNumber != null && !attributePhoneNumber.isEmpty()
							? attributePhoneNumber.get(0)
							: "");
				}

				List<String> rolesUser = new ArrayList<>();
				for (RoleRepresentation rolUser : usersRessource.get(userRepresentation.getId()).roles().getAll()
						.getRealmMappings()) {
					if (Constants.ROLE_ADMIN_SIPNAT.equals(rolUser.getName())
							|| Constants.ROLE_USER_SIPNAT.equals(rolUser.getName())) {
						rolesUser.add(rolUser.getName());
					}
				}

				user.setRoles(rolesUser);
				users.add(user);

			} catch (Exception e) {
				logger.error(e);
			}
		}
		logger.info("End listUsers {}", users.size());
		
		return users;
	}

	public boolean changeStatus(UserDTO userDTO) {
		try {
			RealmResource realmResource = keycloak.realm(keycloakRealm);
			UsersResource usersRessource = realmResource.users();

			UserResource userResource = usersRessource.get(userDTO.getId());

			UserRepresentation userRepresentation = userResource.toRepresentation();

			if (userDTO.isEnabled() != userRepresentation.isEnabled().booleanValue()) {
				userRepresentation.setEnabled(userDTO.isEnabled());
				userResource.update(userRepresentation);
				return true;
			}

			return false;
		} catch (Exception e) {
			throw new ExceptionUserCreation(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
