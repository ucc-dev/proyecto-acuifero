import { NgModule } from '@angular/core';
import { LoginModule } from './authentication/login/login.module';
import { RegisterModule } from './authentication/register/register.module';
import { ForgotPasswordModule } from './authentication/forgot-password/forgot-password.module';
import { SensorModule } from './sensor/sensor.module';
import { ProfileModule } from './profile/profile.module';
import { NotificationsModule } from './notifications/notifications.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { UsersModule } from './users/users.module';

@NgModule({
  imports: [
    LoginModule,
    RegisterModule,
    ForgotPasswordModule,
    SensorModule,
    ProfileModule,
    NotificationsModule,
    DashboardModule,
    UsersModule
  ],
  declarations: []
})
export class PagesModule { }
