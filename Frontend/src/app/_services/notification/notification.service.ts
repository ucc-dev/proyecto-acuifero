import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Notification } from 'app/_model/Notification';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = `${environment.host}/notification`;
  }

  list() {
    return this.http.get<Notification[]>(`${this.endpoint}`);
  }

  getById(id: number) {
    return this.http.get<Notification>(`${this.endpoint}/${id}`)
  }

  updateNotication(notification: any) {
    return this.http.put(`${this.endpoint}`, notification)
  }

  countActiveNotification() {
    return this.http.get(`${this.endpoint}/count`);
  }
}
