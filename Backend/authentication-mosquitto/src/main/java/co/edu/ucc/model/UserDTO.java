package co.edu.ucc.model;

public class UserDTO {

	private String username;
	private String password;
	private String clientid;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	@Override
	public String toString() {
		return "username: [" + username + "], clientid: [" + clientid + "]";
	}

}
