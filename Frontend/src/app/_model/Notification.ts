import { Dato } from "./Dato";

export class Notification {

    id: number;
    idSensor: number;
    sensorName: string;
    values: Dato[];
    dateCreation: Date;
    dateUpgrade: Date;
    typeValueExceeded: string;
    valueExceeded: string;
    type: string;
    title: string;
    detail: string;

    constructor() { }

}