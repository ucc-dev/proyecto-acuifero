package co.edu.ucc.sipnat.mqtt;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.UserMosquittoDTO;
import co.edu.ucc.sipnat.entity.UserMosquitto;
import co.edu.ucc.sipnat.service.ACLMosquittoService;
import co.edu.ucc.sipnat.service.DatoService;
import co.edu.ucc.sipnat.service.UserMosquittoService;
import co.edu.ucc.sipnat.util.Constants;

@Service
@EnableScheduling
public class ClientMqtt implements MqttCallback {

	private Logger logger = LogManager.getLogger(ClientMqtt.class);

	@Value("${mqtt.username_superuser}")
	private String username;
	@Value("${mqtt.password_superuser}")
	private String password;
	@Value("${mqtt.key_secret}")
	private String key;
	@Value("${mqtt.server.url}")
	private String urlServer; // tcp://localhost:1883

	@Autowired
	private ACLMosquittoService aclMosquittoService;
	@Autowired
	private UserMosquittoService userMosquittoService;
	@Autowired
	private DatoService datoService;

	private MqttClient client;

	@PostConstruct
	public void init() {
		try {
			validateSuperuser();

			client = new MqttClient(urlServer, "app-spring");
			MqttConnectOptions connectOptions = new MqttConnectOptions();
			connectOptions.setUserName(username);
			connectOptions.setPassword(password.toCharArray());
			connectOptions.setAutomaticReconnect(false);
			connectOptions.setCleanSession(true);
			client.connect(connectOptions);
			client.setCallback(this);

			client.subscribe(getListTopics());

		} catch (MqttException e) {
			logger.error(e.getMessage());
		}
	}

	@Scheduled(cron = "0 */5 * * * ?")
	private void clientDaemon() {
		try {
			if (!client.isConnected()) {
				logger.info("Client is not connected - try init");
				init();
			}
		} catch (Exception e) {
			logger.error("clientDaemon", e);
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		logger.error("Conection Lost", cause);
		try {
			logger.info("try to reconnect");
//			Thread.sleep(5L * 1000);
			client.close(true);
			client.connect();
		} catch (MqttException e) {
			logger.error("Conection Lost", e);
		}
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		logger.info("----------messageArrived------------");
		logger.info("{}|{}|{}", topic, message, new Date());
		datoService.receiveData(topic, message.toString(), Constants.MQTT);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		logger.info("----------deliveryComplete------------");
		logger.info(token);
	}

	private String[] getListTopics() {
		List<String> topics = aclMosquittoService.listTopics();
		return topics.stream().toArray(String[]::new);
	}

	private void validateSuperuser() {
		UserMosquitto userMosquitto = userMosquittoService.find(username, password);
		if (userMosquitto == null) {
			UserMosquittoDTO userMosquittoDTO = new UserMosquittoDTO();
			userMosquittoDTO.setUsername(username);
			userMosquittoDTO.setPassword(password);
			userMosquittoDTO.setSuperuser(true);

			userMosquittoService.create(userMosquittoDTO);
		}
	}

	public void subscribeTopic(String topic) {
		try {
			if (client != null && client.isConnected()) {
				client.subscribe(topic);
			}
		} catch (MqttException e) {
			logger.error("subscribeTopic", e);
		}
	}

}
