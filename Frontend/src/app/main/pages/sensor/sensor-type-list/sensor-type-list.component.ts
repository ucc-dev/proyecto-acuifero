import { Component, OnInit } from '@angular/core';
import { SensorType } from 'app/_model/SensorType';
import { SensorTypeService } from 'app/_services/sensorType/sensor-type.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-sensor-type-list',
  templateUrl: './sensor-type-list.component.html',
  styleUrls: ['./sensor-type-list.component.scss']
})
export class SensorTypeListComponent implements OnInit {

  dataSource: SensorType[] = [];
  displayedColumns = ['id', 'name', 'magnitude', 'unitMeasure', 'icon'];

  constructor(private sensorType: SensorTypeService) { }

  ngOnInit(): void {
    this.sensorType.listSensorType().subscribe(resp => {
      this.dataSource = resp;
    });
  }

  urlIcon(id: number) {
    return `${environment.host}/sensortype/icon/${id}`;
  }

}
