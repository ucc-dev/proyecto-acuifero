package co.edu.ucc.sipnat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.Notificacion;

@Repository
public interface INotificationDAO extends JpaRepository<Notificacion, Long> {

	@Query(value = "select n from Notificacion n where n.sensor.id = :idSensor and n.revisada = false")
	Notificacion findExistBySensor(@Param("idSensor") Long idSensor);

	@Query(value = "select COUNT(n) from Notificacion n where n.revisada = false")
	Long countActiveNotification();

}
