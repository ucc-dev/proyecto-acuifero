package co.edu.ucc.sipnat.dto;

import java.util.Date;
import java.util.List;

public class NotificationDTO {

	private Long id;
	private Long idSensor;
	private String sensorName;
	private List<DatoDTO> values;
	private Date dateCreation;
	private Date dateUpgrade;
	private String typeValueExceeded;
	private String valueExceeded;
	private String type;
	private String title;
	private String detail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdSensor() {
		return idSensor;
	}

	public void setIdSensor(Long idSensor) {
		this.idSensor = idSensor;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public List<DatoDTO> getValues() {
		return values;
	}

	public void setValues(List<DatoDTO> values) {
		this.values = values;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateUpgrade() {
		return dateUpgrade;
	}

	public void setDateUpgrade(Date dateUpgrade) {
		this.dateUpgrade = dateUpgrade;
	}

	public String getTypeValueExceeded() {
		return typeValueExceeded;
	}

	public void setTypeValueExceeded(String typeValueExceeded) {
		this.typeValueExceeded = typeValueExceeded;
	}

	public String getValueExceeded() {
		return valueExceeded;
	}

	public void setValueExceeded(String valueExceeded) {
		this.valueExceeded = valueExceeded;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
