package co.edu.ucc.sipnat.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Transient;

@Entity
@SuppressWarnings("serial")
public class TipoSensor extends CamposComunesdeEntidad implements Serializable {

	private String nombre;
	private String unidadDeMedida;
	@Lob
	private byte[] logoDelTipo;
	@Transient
	private String uuid;
	@Transient
	private Boolean selecionado;
	private String Magnitud;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUnidadDeMedida() {
		return unidadDeMedida;
	}

	public void setUnidadDeMedida(String unidadDeMedida) {
		this.unidadDeMedida = unidadDeMedida;
	}

	public byte[] getLogoDelTipo() {
		return logoDelTipo;
	}

	public void setLogoDelTipo(byte[] logoDelTipo) {
		this.logoDelTipo = logoDelTipo;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getMagnitud() {
		return Magnitud;
	}

	public void setMagnitud(String magnitud) {
		Magnitud = magnitud;
	}

}
