import {
    AfterViewChecked,
    Component,
    ElementRef,
    Inject,
    OnInit,
    Renderer2,
    ViewChild,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Sensor } from "app/_model/Sensor";
import { environment } from "environments/environment";
import * as L from "leaflet";

@Component({
    selector: "app-modal-map-sensor",
    templateUrl: "./modal-map-sensor.component.html",
    styleUrls: ["./modal-map-sensor.component.scss"],
})
export class ModalMapSensorComponent implements OnInit, AfterViewChecked {
    @ViewChild("view", { read: ElementRef, static: false })
    elementView: ElementRef;

    @ViewChild("map", { static: false })
    map2: ElementRef;

    map: L.Map;

    options: L.MapOptions = {
        layers: [
            L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                minZoom: 0,
                maxZoom: 19,
                attribution: "",
            }),
        ],
        zoom: 17,
        center: L.latLng(0, 0),
        maxZoom: 19,
        zoomAnimation: true,
    };

    constructor(
        public dialogRef: MatDialogRef<ModalMapSensorComponent>,
        private renderer: Renderer2,
        @Inject(MAT_DIALOG_DATA) public data: { sensor: Sensor }
    ) {}

    ngOnInit(): void {}

    ngAfterViewChecked(): void {
        let height = this.elementView.nativeElement.offsetHeight;
        this.renderer.setStyle(
            this.map2.nativeElement,
            "height",
            height + "px"
        );
        this.map.invalidateSize();
    }

    onMapReady(event: any) {
        this.map = event;
        this.addMarkerToMap();
    }

    addMarkerToMap() {
        let icon = L.icon({
            iconUrl: `${environment.host}/sensortype/icon/${this.data.sensor.idSensorType}`,
            iconSize: [48, 48],
            popupAnchor: [0, -24],
        });

        let options: L.MarkerOptions = {
            icon: icon,
        };

        let latitude = parseFloat(this.data.sensor.latitude);
        let longitude = parseFloat(this.data.sensor.longitude);

        L.marker(new L.LatLng(latitude, longitude), options)
            .addTo(this.map)
            .bindPopup(`Posición del sensor ${this.data.sensor.name}`);

        this.map.panTo(new L.LatLng(latitude, longitude));
    }
}
