import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Sensor } from "app/_model/Sensor";
import { SensorService } from "app/_services/sensor/sensor.service";
import { ModalGraphDataSensorComponent } from "../modal-graph-data-sensor/modal-graph-data-sensor.component";
import { ModalMapSensorComponent } from "../modal-map-sensor/modal-map-sensor.component";
import * as moment from 'moment';

@Component({
    selector: "app-sensor-list",
    templateUrl: "./sensor-list.component.html",
    styleUrls: ["./sensor-list.component.scss"],
})
export class SensorListComponent implements OnInit {
    dataSource: Sensor[] = [];
    displayedColumns = [
        "id",
        "name",
        "protocol",
        "magnitude",
        "unitMeasure",
        "topic",
        "status",
        "dateLastData",
        "options",
    ];

    constructor(
        private sensorService: SensorService,
        private dialog: MatDialog
    ) { }

    ngOnInit(): void {
        moment.locale('es');

        this.sensorService.list().subscribe((resp) => {
            console.log(resp);
            this.dataSource = resp;
        });
    }

    showModalMap(sensor: any) {
        this.dialog.open(ModalMapSensorComponent, {
            height: "80%",
            width: "80%",
            data: { sensor: sensor },
            panelClass: "custom-dialog-container",
        });
    }

    showModalGraphData(sensor: any) {
        this.dialog.open(ModalGraphDataSensorComponent, {
            height: "80%",
            width: "80%",
            data: { sensor: sensor },
        });
    }

    formatDateFromNow(date) {
        if (date == undefined || date == null) {
            return '';
        }

        return moment(date).fromNow()
    }
}
