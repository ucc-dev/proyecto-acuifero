package co.edu.ucc.sipnat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.IDatoNotificacionDAO;
import co.edu.ucc.sipnat.dao.INotificationDAO;
import co.edu.ucc.sipnat.dto.NotificationDTO;
import co.edu.ucc.sipnat.entity.Dato;
import co.edu.ucc.sipnat.entity.DatoNotificacion;
import co.edu.ucc.sipnat.entity.Notificacion;
import co.edu.ucc.sipnat.entity.Sensor;
import co.edu.ucc.sipnat.mapper.NotificationMapper;
import co.edu.ucc.sipnat.util.Constants;

@Service
@EnableScheduling
public class NotificationService {

	private Logger logger = LogManager.getLogger(NotificationService.class);

	@Autowired
	private SensorService sensorService;
	@Autowired
	private DatoService datoService;

	@Autowired
	private INotificationDAO notificationDAO;
	@Autowired
	private IDatoNotificacionDAO datoNotificacionDAO;

	@Autowired
	private NotificationMapper notificationMapper;

	public List<NotificationDTO> getList() {

		return notificationMapper
				.entityListToDtoList(notificationDAO.findAll(Sort.by(Sort.Direction.DESC, "fechaCreacion")));
	}

	public NotificationDTO get(Long id) {
		Optional<Notificacion> optional = notificationDAO.findById(id);
		if (optional.isPresent()) {
			return notificationMapper.entityToDto(optional.get());
		} else {
			return null;
		}
	}

	@Scheduled(cron = "0 */5 * * * ?")
	private void verifyReceivedData() {
		logger.info("verifyReceivedData");
		List<Sensor> sensorsWithLimits = new ArrayList<>();
		for (Sensor s : sensorService.listSensorsEntity()) {
			if (s.getMinimo() != 0 && s.getMaximo() != 0) {
				sensorsWithLimits.add(s);
			}
		}

		for (Sensor s : sensorsWithLimits) {
			for (Dato d : datoService.getAllDataNotRevised(s)) {
				try {
					Double value = Double.parseDouble(d.getValor());
					if (value < s.getMinimo()) {
						createNotificacion(s, d, Constants.SENSOR_LIMITE_INFERIOR, Double.toString(s.getMinimo()));
					}

					if (value > s.getMaximo()) {
						createNotificacion(s, d, Constants.SENSOR_LIMITE_SUPERIOR, Double.toString(s.getMaximo()));
					}

					d.setRevisado(true);
					datoService.updateDato(d);
				} catch (Exception e) {
					logger.error("verifyReceivedData", e);
				}
			}
		}
	}

	private void createNotificacion(Sensor sensor, Dato d, String limit, String valueLimit) {
		Notificacion notificacion = notificationDAO.findExistBySensor(sensor.getId());
		if (notificacion == null) {
			notificacion = new Notificacion();

			notificacion.setSensor(sensor);
			notificacion.setTipoNotificacion(Constants.TIPO_NOTIFICACION_ROJA);
			notificacion.setLimiteSuperado(limit);
			notificacion.setValorExcedido(valueLimit);
			notificacion.setRevisada(false);
			notificacion.setTitulo("Valor " + limit + " superado");
			notificacion.setDetalle("Fue recibido un dato fuera del límite " + limit + " parametrizado   en el sensor "
					+ sensor.getDescripcion() + "a las  " + d.getFechaRecoleccion()
					+ ", se recomienda revisar las lecturas obtenidas en la información del sensor.");

			notificacion = notificationDAO.save(notificacion);

			if (notificacion != null) {
				DatoNotificacion datoNotificacion = new DatoNotificacion();
				datoNotificacion.setDato(d);
				datoNotificacion.setNotificacion(notificacion);

				datoNotificacionDAO.save(datoNotificacion);

			}
		} else {
			notificacion.setFechaActualizacion(new Date());
			notificacion.setDetalle("Se ha recibido mas de una lectura fuera del límite " + limit
					+ " parametrizado en el sensor " + sensor.getDescripcion() + "a las " + d.getFechaRecoleccion()
					+ ", se recomienda revisar las lecturas obtenidas en la información del sensor.");

			notificacion = notificationDAO.save(notificacion);

			if (notificacion != null) {
				DatoNotificacion datoNotificacion = new DatoNotificacion();
				datoNotificacion.setDato(d);
				datoNotificacion.setNotificacion(notificacion);

				datoNotificacionDAO.save(datoNotificacion);
			}
		}
	}

	public void updateNotification(NotificationDTO notificationDTO) {
		Optional<Notificacion> optional = notificationDAO.findById(notificationDTO.getId());

		if (optional.isPresent()) {
			Notificacion notificacion = optional.get();

			notificacion.setTipoNotificacion(notificationDTO.getType());
			notificacion.setFechaAnulacion(new Date());
			notificacion.setRevisada(true);

			notificationDAO.save(notificacion);
		}

	}

	public Long countActiveNotification() {
		return notificationDAO.countActiveNotification();
	}
}
