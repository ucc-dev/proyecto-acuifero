package co.edu.ucc.sipnat.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.ISensorDAO;
import co.edu.ucc.sipnat.dto.ACLMosquittoDTO;
import co.edu.ucc.sipnat.dto.SensorDTO;
import co.edu.ucc.sipnat.dto.SensorDetailDTO;
import co.edu.ucc.sipnat.dto.UserMosquittoDTO;
import co.edu.ucc.sipnat.entity.AclMosquitto;
import co.edu.ucc.sipnat.entity.Sensor;
import co.edu.ucc.sipnat.entity.TipoSensor;
import co.edu.ucc.sipnat.entity.UserMosquitto;
import co.edu.ucc.sipnat.mapper.SensorMapper;
import co.edu.ucc.sipnat.mqtt.ClientMqtt;
import co.edu.ucc.sipnat.util.Constants;
import co.edu.ucc.sipnat.util.Utils;

@Service
@EnableScheduling
public class SensorService {

	private Logger logger = LogManager.getLogger(SensorService.class);

	@Autowired
	private UserMosquittoService userMosquittoService;
	@Autowired
	private SensorTypeService sensorTypeService;
	@Autowired
	private ACLMosquittoService aclMosquittoService;
	@Autowired
	private DatoService datoService;
	@Autowired
	private ClientMqtt mqtt;

	@Autowired
	private SensorMapper sensorMapper;
	@Autowired
	private ISensorDAO sensorDAO;

	public Sensor create(SensorDTO dto) {
		Sensor sensor = sensorDAO.save(sensorMapper.dtoToEntity(dto));

		if (dto.getProtocol().equals(Constants.MQTT)) {
			UserMosquitto userMosquitto = createUserMosquitto(dto);
			if (userMosquitto != null) {
				AclMosquitto aclMosquitto = createACLMosquitto(dto, userMosquitto, sensor);
				sensor.setTopic(aclMosquitto.getTopic());

				mqtt.subscribeTopic(aclMosquitto.getTopic());
			}
		}

		return sensor;
	}

	public Sensor getById(Long idSensor) {
		Optional<Sensor> optional = sensorDAO.findById(idSensor);
		return optional.isPresent() ? optional.get() : null;
	}

	public List<SensorDetailDTO> listSensors() {
		return sensorMapper.entityToDto(sensorDAO.findAll());
	}

	public List<Sensor> listSensorsEntity() {
		return sensorDAO.findAll();
	}

	private UserMosquitto createUserMosquitto(SensorDTO dto) {
		UserMosquittoDTO userMosquittoDTO = new UserMosquittoDTO();

		userMosquittoDTO.setUsername(dto.getUser());
		userMosquittoDTO.setPassword(dto.getPassword());
		userMosquittoDTO.setSuperuser(false);

		return userMosquittoService.create(userMosquittoDTO);
	}

	private AclMosquitto createACLMosquitto(SensorDTO dto, UserMosquitto userMosquitto, Sensor sensor) {
		TipoSensor tipoSensor = sensorTypeService.getById(dto.getIdSensorType());
		ACLMosquittoDTO aclMosquittoDTO = new ACLMosquittoDTO();
		aclMosquittoDTO.setUserMosquitto(userMosquitto);
		aclMosquittoDTO.setSensor(sensor);
		aclMosquittoDTO.setTopic(Utils.buildTopic(dto.getName(), tipoSensor.getMagnitud()));
		aclMosquittoDTO.setSuscriptionPermission(true);
		aclMosquittoDTO.setWritePermission(true);
		aclMosquittoDTO.setReadPermission(false);

		return aclMosquittoService.create(aclMosquittoDTO);
	}

	@Scheduled(cron = "0 */20 * * * ?")
	private void upgradeStatesSensors() {
		logger.info("upgradeStatesSensors");
		for (Sensor s : listSensorsEntity()) {
			Date dateLastData = datoService.dateLastData(s);
			if (dateLastData != null) {
				Date date = new Date(new Date().getTime() - 60 * 60 * 1000);

				if (dateLastData.after(date)) {
					s.setEstadoDelSensor(Constants.SENSOR_ESTADO_CONECTADO);
					s.setFechaDeDesconexion(null);
					sensorDAO.save(s);
				}

				if (dateLastData.before(date)) {
					s.setEstadoDelSensor(Constants.SENSOR_ESTADO_DESCONECTADO);
					s.setFechaDeDesconexion(dateLastData);
					sensorDAO.save(s);
				}
			}
		}
	}

}
