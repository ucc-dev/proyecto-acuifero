package co.edu.ucc.model;

public class AuthorizationAclDTO extends UserDTO {

	private String topic;
	private Object acc;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Object getAcc() {
		return acc;
	}

	public void setAcc(Object acc) {
		this.acc = acc;
	}

	@Override
	public String toString() {
		return  "username: [" + getUsername() + "], clientid: [" + getClientid() + "], topic: [" + topic + "], acc: [" + acc + "]";
	}

}
