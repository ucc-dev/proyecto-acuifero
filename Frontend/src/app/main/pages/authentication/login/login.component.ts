import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import {
    SIPNAT_ACCESS_TOKEN,
    SIPNAT_EXPIRES_IN,
    SIPNAT_REFRESH_EXPIRES_IN,
    SIPNAT_REFRESH_TOKEN,
    SIPNAT_SCOPE,
    SIPNAT_SESSION_STATE,
    SIPNAT_TOKEN_TYPE,
} from "app/_utils/constants";
import { AuthenticationService } from "app/_services/authentication/authentication.service";
import { Router } from "@angular/router";
import { User } from "app/_model/User";
import swal from "sweetalert2";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";

@Component({
    selector: "login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private router: Router,
        private _fuseNavigationService: FuseNavigationService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true,
                },
                toolbar: {
                    hidden: true,
                },
                footer: {
                    hidden: true,
                },
                sidepanel: {
                    hidden: true,
                },
            },
        };
    }

    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            email: ["", [Validators.required, Validators.email]],
            password: ["", Validators.required],
        });
    }

    get controls() {
        return this.loginForm.controls;
    }

    onSubmit() {
        if (this.loginForm.valid) {
            let user: User = new User();
            user.email = this.controls.email.value;
            user.password = this.controls.password.value;

            this.authenticationService.signin(user).subscribe(
                (resp) => {
                    console.log(resp);

                    localStorage.setItem(
                        SIPNAT_ACCESS_TOKEN,
                        resp.access_token
                    );
                    localStorage.setItem(
                        SIPNAT_REFRESH_TOKEN,
                        resp.refresh_token
                    );
                    localStorage.setItem(SIPNAT_TOKEN_TYPE, resp.token_type);
                    localStorage.setItem(
                        SIPNAT_SESSION_STATE,
                        resp.session_state
                    );
                    localStorage.setItem(SIPNAT_SCOPE, resp.scope);
                    localStorage.setItem(
                        SIPNAT_EXPIRES_IN,
                        resp.expires_in.toString()
                    );
                    localStorage.setItem(
                        SIPNAT_REFRESH_EXPIRES_IN,
                        resp.refresh_expires_in.toString()
                    );

                    this.inicilizateMenu(resp.access_token);

                    this.router.navigate(["pages/sensor-type"]);
                }, (error) => {
                    if (error?.status == 401) {
                        console.log("por favor verifique sus credenciales");
                        swal.fire('Error al ingresar', 'Verifique sus credenciales e intente nuevamente', 'error');

                    } else {
                        console.log(error);
                        swal.fire('Error al ingresar', 'Ha ocurrido un error inesperado, intente ingresar mas tarde', 'error');
                    }
                }
            );
        }
    }

    inicilizateMenu(accessToken: string) {
        let roles: string[] = this.authenticationService.getRoleAccessToken(accessToken);
        console.log(roles);
        if (this.rolesToString(roles) == "admin_sipnat") {
            console.log("user admini")
        } else {
            this._fuseNavigationService.removeNavigationItem('crear-tipo-sensor');
            this._fuseNavigationService.removeNavigationItem('crear-sensor');
            this._fuseNavigationService.removeNavigationItem('users');
            this._fuseNavigationService.removeNavigationItem('dashboards');
        }
    }

    rolesToString(roles: string[]): string {
        let otherRole: string;
        for (let item of roles) {
            if (item == "admin_sipnat") {
                return item;
            } else {
                item = otherRole
            }
        }

        return otherRole;
    }
}
