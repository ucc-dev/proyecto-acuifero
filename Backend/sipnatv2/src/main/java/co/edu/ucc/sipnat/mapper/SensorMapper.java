package co.edu.ucc.sipnat.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.SensorDTO;
import co.edu.ucc.sipnat.dto.SensorDetailDTO;
import co.edu.ucc.sipnat.entity.Sensor;
import co.edu.ucc.sipnat.entity.TipoSensor;
import co.edu.ucc.sipnat.service.ACLMosquittoService;
import co.edu.ucc.sipnat.service.DatoService;
import co.edu.ucc.sipnat.service.SensorTypeService;
import co.edu.ucc.sipnat.util.Constants;

@Service
public class SensorMapper {

	@Autowired
	private SensorTypeService sensorTypeService;
	@Autowired
	private DatoService datoService;
	@Autowired
	private ACLMosquittoService aclMosquittoService;

	public Sensor dtoToEntity(SensorDTO dto) {
		TipoSensor tipoSensor = sensorTypeService.getById(dto.getIdSensorType());
		Sensor sensor = new Sensor();

		sensor.setFechaCreacion(new Date());
		sensor.setEstadoDelSensor(Constants.SENSOR_ESTADO_CREADO);
		sensor.setLatitud(dto.getLatitude());
		sensor.setLongitud(dto.getLongitude());
		if (dto.getMaxLimit() != null && dto.getMinLimit() != null) {
			sensor.setMinimo(dto.getMinLimit());
			sensor.setMaximo(dto.getMaxLimit());
		}
		sensor.setProtocolo(dto.getProtocol());
		sensor.setTipoSensor(tipoSensor);
		sensor.setDescripcion(dto.getName());

		return sensor;
	}

	public List<SensorDetailDTO> entityToDto(List<Sensor> sensors) {
		List<SensorDetailDTO> sensorDetailDTOs = new ArrayList<>();

		for (Sensor s : sensors) {
			SensorDetailDTO detailDTO = new SensorDetailDTO();

			detailDTO.setId(s.getId());
			detailDTO.setProtocol(s.getProtocolo());
			detailDTO.setLatitude(s.getLatitud());
			detailDTO.setLongitude(s.getLongitud());
			detailDTO.setIdSensorType(s.getTipoSensor().getId());
			detailDTO.setSensorTypeName(s.getTipoSensor().getNombre());
			detailDTO.setMagnitude(s.getTipoSensor().getMagnitud());
			detailDTO.setUnitMeasure(s.getTipoSensor().getUnidadDeMedida());
			detailDTO.setDateLastData(getDateLastData(s));
			detailDTO.setStatus(s.getEstadoDelSensor());
			detailDTO.setMaxLimit(s.getMaximo());
			detailDTO.setMinLimit(s.getMinimo());
			detailDTO.setTopic(getTopicBySensor(s));
			detailDTO.setName(s.getDescripcion());
			
			sensorDetailDTOs.add(detailDTO);
		}

		return sensorDetailDTOs;
	}

	private Date getDateLastData(Sensor sensor) {
		return datoService.dateLastData(sensor);
	}

	private String getTopicBySensor(Sensor sensor) {
		return aclMosquittoService.topicBySensor(sensor.getId());
	}
	
	

}
