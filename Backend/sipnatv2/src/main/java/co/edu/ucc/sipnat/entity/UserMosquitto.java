package co.edu.ucc.sipnat.entity;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class UserMosquitto extends CamposComunesdeEntidad {

	private String username;
	private String password;
	private boolean superuser;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSuperuser() {
		return superuser;
	}

	public void setSuperuser(boolean superuser) {
		this.superuser = superuser;
	}

}
