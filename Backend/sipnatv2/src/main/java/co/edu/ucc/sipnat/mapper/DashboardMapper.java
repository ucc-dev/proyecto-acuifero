package co.edu.ucc.sipnat.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.DashboardDTO;
import co.edu.ucc.sipnat.entity.Tablero;

@Service
public class DashboardMapper {

	public Tablero dtoToEntity(DashboardDTO dto) {
		Tablero tablero = new Tablero();

		tablero.setNombre(dto.getName());

		return tablero;
	}

	public DashboardDTO entityToDto(Tablero tablero) {
		DashboardDTO dto = new DashboardDTO();

		dto.setId(tablero.getId());
		dto.setName(tablero.getNombre());

		return dto;
	}

	public List<DashboardDTO> entityListToListDTO(List<Tablero> tableros) {
		List<DashboardDTO> listDtos = new ArrayList<>();

		for (Tablero t : tableros) {
			listDtos.add(entityToDto(t));
		}

		return listDtos;
	}

}
