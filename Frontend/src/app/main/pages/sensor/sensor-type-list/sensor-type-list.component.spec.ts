import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorTypeListComponent } from './sensor-type-list.component';

describe('SensorTypeListComponent', () => {
  let component: SensorTypeListComponent;
  let fixture: ComponentFixture<SensorTypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensorTypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
