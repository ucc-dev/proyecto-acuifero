package co.edu.ucc.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import co.edu.ucc.model.ExceptionResponseDTO;

@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> manejarTodasExcepciones(Exception ex, WebRequest request) {
		ExceptionResponseDTO exceptionResponse = new ExceptionResponseDTO(new Date(), ex.getMessage(),
				request.getDescription(true));
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ExceptionUserAuthentication.class)
	public final ResponseEntity<Object> manejarModeloExcepciones(ExceptionUserAuthentication ex, WebRequest request) {
		ExceptionResponseDTO exceptionResponse = new ExceptionResponseDTO(new Date(), ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse, ex.getHttpStatus());
	}
}
