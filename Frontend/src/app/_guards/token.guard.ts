import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthenticationService } from 'app/_services/authentication/authentication.service';
import { SIPNAT_REFRESH_TOKEN } from 'app/_utils/constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenGuard implements CanActivate {

  constructor(
    private router: Router,
    private authentication: AuthenticationService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    console.log("aqui en can active")
    let accessToken: string = sessionStorage.getItem(SIPNAT_REFRESH_TOKEN)
    if (!this.authentication.refreshTokenExpiration) {
      this.authentication.logout();
    }
    return true;
  }

}
