package co.edu.ucc.sipnat.dto;

import java.util.Date;

public class DatoDTO {

	private Date fechaRecolecion;
	private String horaRecolecion;
	private String dato;
	private SensorDTO sensor;

	public Date getFechaRecolecion() {
		return fechaRecolecion;
	}

	public void setFechaRecolecion(Date fechaRecolecion) {
		this.fechaRecolecion = fechaRecolecion;
	}

	public String getHoraRecolecion() {
		return horaRecolecion;
	}

	public void setHoraRecolecion(String horaRecolecion) {
		this.horaRecolecion = horaRecolecion;
	}

	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

}
