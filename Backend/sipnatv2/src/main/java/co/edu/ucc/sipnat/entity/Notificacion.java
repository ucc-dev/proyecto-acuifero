package co.edu.ucc.sipnat.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class Notificacion extends CamposComunesdeEntidad {

	@ManyToOne
	private Sensor sensor;
	private String tipoNotificacion;
	private String limiteSuperado;
	private String valorExcedido;
	private boolean revisada;
	private Date fechaActualizacion;
	private String titulo;
	private String detalle;

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public String getTipoNotificacion() {
		return tipoNotificacion;
	}

	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}

	public String getLimiteSuperado() {
		return limiteSuperado;
	}

	public void setLimiteSuperado(String limiteSuperado) {
		this.limiteSuperado = limiteSuperado;
	}

	public String getValorExcedido() {
		return valorExcedido;
	}

	public void setValorExcedido(String valorExcedido) {
		this.valorExcedido = valorExcedido;
	}

	public boolean isRevisada() {
		return revisada;
	}

	public void setRevisada(boolean revisada) {
		this.revisada = revisada;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

}
