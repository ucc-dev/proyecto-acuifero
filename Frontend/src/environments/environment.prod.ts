export const environment = {
    production: true,
    hmr       : false,
    host: 'https://api.sipnat.co'
};
