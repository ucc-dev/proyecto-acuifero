package co.edu.ucc.sipnat.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@SuppressWarnings("serial")
public class Sensor extends CamposComunesdeEntidad implements Serializable {

	private String codigoHexadecimal;
	private String latitud;
	private String longitud;
	private String estadoDelSensor;
	@Column(columnDefinition = "TEXT")
	private String descripcion;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaDeDesconexion;
	@ManyToOne
	private TipoSensor tipoSensor;
	private double minimo;
	private double maximo;
	private String protocolo;
	@Transient
	private String topic;

	public String getCodigoHexadecimal() {
		return codigoHexadecimal;
	}

	public void setCodigoHexadecimal(String codigoHexadecimal) {
		this.codigoHexadecimal = codigoHexadecimal;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getEstadoDelSensor() {
		return estadoDelSensor;
	}

	public void setEstadoDelSensor(String estadoDelSensor) {
		this.estadoDelSensor = estadoDelSensor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaDeDesconexion() {
		return fechaDeDesconexion;
	}

	public void setFechaDeDesconexion(Date fechaDeDesconexion) {
		this.fechaDeDesconexion = fechaDeDesconexion;
	}

	public TipoSensor getTipoSensor() {
		return tipoSensor;
	}

	public void setTipoSensor(TipoSensor tipoSensor) {
		this.tipoSensor = tipoSensor;
	}

	public double getMinimo() {
		return minimo;
	}

	public void setMinimo(double minimo) {
		this.minimo = minimo;
	}

	public double getMaximo() {
		return maximo;
	}

	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}

	public String getProtocolo() {
		return protocolo;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
