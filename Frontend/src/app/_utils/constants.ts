export const SIPNAT_ACCESS_TOKEN = 'access_token';
export const SIPNAT_REFRESH_TOKEN = 'refresh_token';
export const SIPNAT_TOKEN_TYPE = 'token_type';
export const SIPNAT_SESSION_STATE = 'session_state';
export const SIPNAT_SCOPE = 'scope';
export const SIPNAT_EXPIRES_IN = 'expires_in';
export const SIPNAT_REFRESH_EXPIRES_IN = 'refresh_expires_in';

export const SENSOR_STATE_CREATED = 'CREADO';
export const SENSOR_STATE_CONNECTED = 'CONECTADO';
export const SENSOR_STATE_DISCONNECTED = 'DESCONECTADO';
