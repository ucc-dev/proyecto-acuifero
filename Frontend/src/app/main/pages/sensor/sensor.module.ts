import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { FuseSharedModule } from "@fuse/shared.module";
import { MatTableModule } from "@angular/material/table";

import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { NgxMatFileInputModule } from "@angular-material-components/file-input";

import { MatPaginatorModule } from "@angular/material/paginator";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { SensorTypeListComponent } from "./sensor-type-list/sensor-type-list.component";
import { SensorTypeComponent } from "./sensor-type/sensor-type.component";
import { SensorComponent } from "./sensor/sensor.component";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { LeafletDrawModule } from "@asymmetrik/ngx-leaflet-draw";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { SensorListComponent } from "./sensor-list/sensor-list.component";
import { ModalMapSensorComponent } from "./modal-map-sensor/modal-map-sensor.component";
import { MatDialogModule } from "@angular/material/dialog";
import { ModalGraphDataSensorComponent } from "./modal-graph-data-sensor/modal-graph-data-sensor.component";
import { ChartsModule } from "ng2-charts";
import { NgxChartsModule } from "@swimlane/ngx-charts";

const routes = [
    {
        path: "sensor-type-list",
        component: SensorTypeListComponent,
    },
    {
        path: "sensor-type",
        component: SensorTypeComponent,
    },
    {
        path: "sensor",
        component: SensorComponent,
    },
    {
        path: "sensor-list",
        component: SensorListComponent,
    },
];

@NgModule({
    declarations: [
        SensorTypeListComponent,
        SensorTypeComponent,
        SensorComponent,
        SensorListComponent,
        ModalMapSensorComponent,
        ModalGraphDataSensorComponent,
    ],
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,
        FuseSharedModule,

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatSlideToggleModule,
        NgxMatFileInputModule,
        NgxMatSelectSearchModule,
        MatDialogModule,

        MatTableModule,
        MatPaginatorModule,

        LeafletModule,
        LeafletDrawModule,
        ChartsModule,
        NgxChartsModule,
    ],
    entryComponents: [ModalMapSensorComponent, ModalGraphDataSensorComponent],
    exports: [SensorTypeListComponent, SensorTypeComponent, SensorComponent],
})
export class SensorModule {}
