import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Sensor } from 'app/_model/Sensor';
import { ModalGraphDataSensorComponent } from '../../sensor/modal-graph-data-sensor/modal-graph-data-sensor.component';
import { ModalMapSensorComponent } from '../../sensor/modal-map-sensor/modal-map-sensor.component';
import * as moment from 'moment';
import { SensorService } from 'app/_services/sensor/sensor.service';
import { DashboardService } from 'app/_services/dashboard/dashboard.service';
import { Dashboard } from 'app/_model/Dashboard';
import swal from "sweetalert2";

@Component({
  selector: 'app-dashboard-create',
  templateUrl: './dashboard-create.component.html',
  styleUrls: ['./dashboard-create.component.scss']
})
export class DashboardCreateComponent implements OnInit {
  formStep1: FormGroup;
  formStep2: FormGroup;

  dataSource: Sensor[] = [];
  displayedColumns = [
    "select",
    "id",
    "name",
    "protocol",
    "magnitude",
    "unitMeasure",
    "status",
    "dateLastData",
    "options",
  ];

  selectSensors: any[];

  constructor(private formBuilder: FormBuilder, private dialog: MatDialog, private sensorService: SensorService, private dashboardService: DashboardService) { }

  ngOnInit(): void {
    moment.locale('es');

    this.sensorService.list().subscribe((resp) => {
      this.dataSource = resp;
    });

    this.inicializateFormStep1();
    this.inicializateFormStep2();

    this.selectSensors = new Array();
  }

  inicializateFormStep1() {
    this.formStep1 = this.formBuilder.group({
      name: ["", Validators.required],
    });
  }

  inicializateFormStep2() {
    this.formStep2 = this.formBuilder.group({
      temp: ["", Validators.required]
    });
  }

  showModalMap(sensor: any) {
    this.dialog.open(ModalMapSensorComponent, {
      height: "80%",
      width: "80%",
      data: { sensor: sensor },
      panelClass: "custom-dialog-container",
    });
  }

  showModalGraphData(sensor: any) {
    this.dialog.open(ModalGraphDataSensorComponent, {
      height: "80%",
      width: "80%",
      data: { sensor: sensor },
    });
  }

  formatDateFromNow(date) {
    if (date == undefined || date == null) {
      return '';
    }

    return moment(date).fromNow()
  }

  changeCheckBox(event: any, sensor: any) {
    if (event.checked == true) {
      this.selectSensors.push(sensor.id);
    }

    if (event.checked == false) {
      let index = this.selectSensors.indexOf(sensor.id);
      this.selectSensors.splice(index, 1);
    }

    if (this.selectSensors.length == 0) {
      this.formStep2.get("temp").setErrors({ required: true })
    } else {
      this.formStep2.get("temp").setErrors(null);
    }
  }

  finishVerticalStepper() {
    if (this.formStep1.valid && this.formStep2.valid) {
      let dashboard: Dashboard = new Dashboard();
      dashboard.name = this.formStep1.get("name").value;
      dashboard.idSensors = this.selectSensors;

      swal.fire({
        title: "Creando...",
        allowOutsideClick: false,
        allowEscapeKey: false,
      });

      swal.showLoading();

      this.dashboardService.create(dashboard).subscribe(resp => {
        console.log(resp);
        swal.close();
        swal.fire({
          title: 'Tablero ha sido creado con éxito.',
          allowOutsideClick: false,
          allowEscapeKey: false,
          showDenyButton: false,
          showCancelButton: false,
          confirmButtonText: 'Ok',
          icon: 'success'
        }).then((result) => {
          if (result.isConfirmed) {
            this.ngOnInit();
          }
        });
      }, error => {
        console.log(error);
        swal.close();
        swal.fire('Error en la creación', `Se ha presentado un error en la creación del tablero, por favor intente nuevamente`, 'error');
      });
    }
  }

}
