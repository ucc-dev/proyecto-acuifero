package co.edu.ucc.sipnat.dto;

import java.util.List;

public class DashboardDTO {

	private Long id;
	private String name;
	private List<Long> idSensors;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Long> getIdSensors() {
		return idSensors;
	}

	public void setIdSensors(List<Long> idSensors) {
		this.idSensors = idSensors;
	}

}
