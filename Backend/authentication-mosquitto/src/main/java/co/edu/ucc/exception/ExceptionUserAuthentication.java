package co.edu.ucc.exception;

import org.springframework.http.HttpStatus;

public class ExceptionUserAuthentication extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final HttpStatus httpStatus;

	public ExceptionUserAuthentication(String mensaje, HttpStatus status) {
		super(mensaje);
		this.httpStatus = status;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

}
