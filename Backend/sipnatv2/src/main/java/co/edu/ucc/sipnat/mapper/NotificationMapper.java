package co.edu.ucc.sipnat.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.IDatoNotificacionDAO;
import co.edu.ucc.sipnat.dto.DatoDTO;
import co.edu.ucc.sipnat.dto.NotificationDTO;
import co.edu.ucc.sipnat.entity.Dato;
import co.edu.ucc.sipnat.entity.DatoNotificacion;
import co.edu.ucc.sipnat.entity.Notificacion;

@Service
public class NotificationMapper {

	@Autowired
	private IDatoNotificacionDAO notificacionDAO;

	@Autowired
	private DataMapper dataMapper;

	public List<NotificationDTO> entityListToDtoList(List<Notificacion> notificaciones) {
		List<NotificationDTO> notificationDTOs = new ArrayList<>();

		for (Notificacion notificacion : notificaciones) {
			notificationDTOs.add(entityToDto(notificacion));
		}

		return notificationDTOs;
	}

	public NotificationDTO entityToDto(Notificacion notificacion) {
		NotificationDTO dto = new NotificationDTO();

		dto.setId(notificacion.getId());
		dto.setIdSensor(notificacion.getSensor().getId());
		dto.setSensorName(notificacion.getSensor().getDescripcion());
		dto.setDateCreation(notificacion.getFechaCreacion());
		dto.setDateUpgrade(notificacion.getFechaActualizacion());
		dto.setTypeValueExceeded(notificacion.getLimiteSuperado());
		dto.setValueExceeded(notificacion.getValorExcedido());
		dto.setType(notificacion.getTipoNotificacion());
		dto.setTitle(notificacion.getTitulo());
		dto.setDetail(notificacion.getDetalle());
		dto.setValues(getDataByNotification(notificacion.getId()));

		return dto;
	}

	private List<DatoDTO> getDataByNotification(Long idNotification) {
		List<DatoNotificacion> datoNotificacions = notificacionDAO.getAllDataNotification(idNotification);
		List<Dato> response = new ArrayList<>();

		for (DatoNotificacion dn : datoNotificacions) {
			response.add(dn.getDato());
		}

		return dataMapper.listEntityToListDto(response);
	}

}
