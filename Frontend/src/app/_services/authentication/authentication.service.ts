import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AccessTokenResponse } from "app/_model/AccessTokenResponse";
import { User } from "app/_model/User";
import {
    SIPNAT_REFRESH_TOKEN,
    SIPNAT_ACCESS_TOKEN,
} from "app/_utils/constants";
import { environment } from "environments/environment";

@Injectable({
    providedIn: "root",
})
export class AuthenticationService {
    private endpoint: string;

    constructor(private http: HttpClient, private router: Router) {
        this.endpoint = `${environment.host}/users`;
    }

    signin(user: User) {
        return this.http.post<AccessTokenResponse>(`${this.endpoint}/signin`, user);
    }

    signup(user: User) {
        return this.http.post<any>(`${this.endpoint}/signup`, user);
    }

    refreshToken() {
        if (!this.refreshTokenExpiration()) {
            console.log("Cerrar sesion");
        } else {
            let data = {
                refreshToken: localStorage.getItem(SIPNAT_REFRESH_TOKEN),
            };
            return this.http.post<AccessTokenResponse>(`${this.endpoint}/refresh`, data);
        }
    }

    logout(): void {
        let data = {
            accesstoken: localStorage.getItem(SIPNAT_ACCESS_TOKEN),
            refreshToken: localStorage.getItem(SIPNAT_REFRESH_TOKEN),
        };

        localStorage.clear();

        this.http.post<any>(`${this.endpoint}/logout`, data).subscribe((resp) => {
            this.router.navigate(["/"]);
        }, error => {
            this.router.navigate(["/"]);
        });
    }

    listUsers() {
        return this.http.get<any[]>(`${this.endpoint}/list`);
    }


    changeStatus(user: any) {
        return this.http.patch<any>(`${this.endpoint}/changeStatus`, user);
    }

    /**
     * return false if token expired
     */
    accessTokenExpiration(): boolean {
        let tokenData = this.getAccessTokenData();
        let now = new Date().getTime() / 1000;
        return tokenData.exp > now;
    }

    /**
     * return false if token expired
     */
    refreshTokenExpiration(): boolean {
        let tokenData = this.getRefreshTokenData();
        let now = new Date().getTime() / 1000;
        return tokenData.exp > now;
    }

    getAccessTokenData() {
        let token = localStorage.getItem(SIPNAT_ACCESS_TOKEN);
        if (token != null && token != undefined) {
            return JSON.parse(atob(token.split(".")[1]));
        } else {
            return null;
        }
    }

    getRefreshTokenData() {
        let token = localStorage.getItem(SIPNAT_REFRESH_TOKEN);
        if (token != null && token != undefined) {
            return JSON.parse(atob(token.split(".")[1]));
        }
    }

    getRoleAccessToken(accessToken: string) {
        if (accessToken != null && accessToken != undefined) {
            let obj = JSON.parse(atob(accessToken.split(".")[1]));
            return obj?.realm_access?.roles;
        }
    }
}
