package co.edu.ucc.sipnat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.UserMosquitto;

@Repository
public interface IUserMosquittoDAO extends JpaRepository<UserMosquitto, Long> {

	@Query(value = "select u from UserMosquitto u where u.username = :username and u.password = :password")
	public UserMosquitto findByUsernameAndPassword(@Param("username") String username,
			@Param("password") String password);
}
