import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Dashboard } from 'app/_model/Dashboard';
import { Sensor } from 'app/_model/Sensor';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {


  private endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = `${environment.host}/dashboard`;
  }

  create(dashboard: Dashboard) {
    return this.http.post<any>(`${this.endpoint}`, dashboard);
  }

  getProjects() {
    return this.http.get<any[]>(`${this.endpoint}/`);
  }

  getProject(id: number) {
    return this.http.get<any>(`${this.endpoint}/${id}`);
  }

  getSensors(id: number){
    return this.http.get<Sensor[]>(`${this.endpoint}/${id}/sensors/`);
  }

  getDataSensors(id: number, type: string) {
    return this.http.get<any>(`${this.endpoint}/${id}/sensors/data/${type}`);
  }

  countSensorsByState(id: number, state: string) {
    return this.http.get<any>(`${this.endpoint}/${id}/sensors/state/${state}`)
  }

}
