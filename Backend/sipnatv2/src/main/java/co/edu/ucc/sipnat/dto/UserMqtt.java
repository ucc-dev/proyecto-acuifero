//package co.edu.ucc.sipnat.dto;
//
//public class UserMqtt {
//
//	private String username;
//	private String password;
//	private String clientid;
//	private String topic;
//	private Float acc;
//
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public String getClientid() {
//		return clientid;
//	}
//
//	public void setClientid(String clientid) {
//		this.clientid = clientid;
//	}
//
//	public String getTopic() {
//		return topic;
//	}
//
//	public void setTopic(String topic) {
//		this.topic = topic;
//	}
//
//	public Float getAcc() {
//		return acc;
//	}
//
//	public void setAcc(Float acc) {
//		this.acc = acc;
//	}
//
//	@Override
//	public String toString() {
//		return "username: [" + username + "] - password: [" + password + "] - clientid: [" + clientid + "] - topic: ["
//				+ topic + "]";
//	}
//
//}
