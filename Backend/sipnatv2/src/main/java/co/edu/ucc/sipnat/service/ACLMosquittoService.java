package co.edu.ucc.sipnat.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.IACLMosquittoDAO;
import co.edu.ucc.sipnat.dto.ACLMosquittoDTO;
import co.edu.ucc.sipnat.entity.AclMosquitto;
import co.edu.ucc.sipnat.mapper.ACLMosquittoMapper;

@Service
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ACLMosquittoService {

	@Autowired
	private IACLMosquittoDAO dao;
	@Autowired
	private ACLMosquittoMapper mapper;

	public AclMosquitto create(ACLMosquittoDTO aclMosquittoDTO) {
		return create(mapper.dtoToEntity(aclMosquittoDTO));
	}

	public AclMosquitto create(AclMosquitto entity) {
		return dao.save(entity);
	}

	public List<String> listTopics() {
		return dao.findAllTopicByState().stream().map(obj -> obj.getTopic()).collect(Collectors.toList());
	}
	
	public List<AclMosquitto> listACLMosquitto() {
		return dao.findAllTopicByState();
	}
	
	public String topicBySensor(Long idSensor) {
		return dao.findTopicByIdSensor(idSensor);
	}
}
