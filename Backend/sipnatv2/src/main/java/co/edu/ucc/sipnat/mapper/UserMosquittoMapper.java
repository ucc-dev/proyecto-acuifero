package co.edu.ucc.sipnat.mapper;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.SensorDTO;
import co.edu.ucc.sipnat.dto.UserMosquittoDTO;
import co.edu.ucc.sipnat.entity.UserMosquitto;
import co.edu.ucc.sipnat.util.Utils;

@Service
public class UserMosquittoMapper {

	@Value("${mqtt.key_secret}")
	private String clientSecret;

	public UserMosquitto dtoToEntity(SensorDTO dto) {
		UserMosquitto userMosquitto = new UserMosquitto();

		userMosquitto.setUsername(dto.getUser());
		userMosquitto.setPassword(dto.getPassword());
		userMosquitto.setFechaCreacion(new Date());
		userMosquitto.setSuperuser(false);

		return userMosquitto;
	}

	public UserMosquitto dtoToEntity(UserMosquittoDTO dto) {
		String password = Utils.calculateSecretHash(dto.getUsername(), dto.getPassword(), clientSecret);
		UserMosquitto userMosquitto = new UserMosquitto();

		userMosquitto.setUsername(dto.getUsername());
		userMosquitto.setPassword(password);
		userMosquitto.setSuperuser(dto.isSuperuser());
		userMosquitto.setFechaCreacion(new Date());

		return userMosquitto;
	}

}
