package co.edu.ucc.sipnat.exception;

import org.springframework.http.HttpStatus;

public class ExceptionUserCreation extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final HttpStatus httpStatus;

	public ExceptionUserCreation(String mensaje, HttpStatus status) {
		super(mensaje);
		this.httpStatus = status;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

}
