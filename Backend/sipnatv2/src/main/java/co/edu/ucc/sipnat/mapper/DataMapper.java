package co.edu.ucc.sipnat.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.DatoDTO;
import co.edu.ucc.sipnat.dto.response.DataSensorResponseDTO;
import co.edu.ucc.sipnat.entity.Dato;
import co.edu.ucc.sipnat.entity.Sensor;

@Service
public class DataMapper {

	private Logger logger = LogManager.getLogger(DataMapper.class);

	public DataSensorResponseDTO entityToDto(Sensor sensor, List<Dato> datos) {
		DataSensorResponseDTO responseDTO = new DataSensorResponseDTO();

		responseDTO.setUnitMeasure(sensor.getTipoSensor().getUnidadDeMedida());
		responseDTO.setListData(listEntityToListDto(datos));

		return responseDTO;
	}

	public List<DatoDTO> listEntityToListDto(List<Dato> datos) {
		List<DatoDTO> datoDTOs = new ArrayList<>();

		for (Dato d : datos) {
			try {
				DatoDTO datoDTO = new DatoDTO();

				datoDTO.setDato(d.getValor());
				datoDTO.setFechaRecolecion(d.getFechaRecoleccion());

				datoDTOs.add(datoDTO);
			} catch (Exception e) {
				logger.error("listEntityToListDto", e);
			}
		}
		return datoDTOs;

	}

}
