package co.edu.ucc.sipnat.util;

public class Constants {

	private Constants() {
	}

	public static final String MQTT = "MQTT";
	public static final String SECRET = "secret";
	public static final String AUTHORIZATION =  "Authorization";
	public static final String KEYCLOAK_ADMIN_CLI = "admin-cli";
	public static final String KEYCLOAK_ATTRIBUTE_PHONENUMBER = "phoneNumber";
	public static final String AUTHORIZATION_BEARER ="Bearer ";
	
	// Estados Sensor
	public static final String SENSOR_ESTADO_CREADO = "CREADO";
	public static final String SENSOR_ESTADO_CONECTADO = "CONECTADO";
	public static final String SENSOR_ESTADO_DESCONECTADO = "DESCONECTADO";

	// LIMITES SENSOR
	public static final String SENSOR_LIMITE_SUPERIOR = "Máximo";
	public static final String SENSOR_LIMITE_INFERIOR = "Mínimo";

	// TIPO NOTIFICACION
	public static final String TIPO_NOTIFICACION_ROJA = "ROJA";
	
	// unidades de tiempo
	public static final String ALL = "ALL";
	public static final String TODAY = "TODAY";
	
	public static final String ROLE_ADMIN_SIPNAT = "admin_sipnat";
	public static final String ROLE_USER_SIPNAT = "user_sipnat";
}
