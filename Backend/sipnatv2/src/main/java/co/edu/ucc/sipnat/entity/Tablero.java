package co.edu.ucc.sipnat.entity;

import javax.persistence.Entity;

@Entity
@SuppressWarnings("serial")
public class Tablero extends CamposComunesdeEntidad {

	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
