import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Sensor } from "app/_model/Sensor";
import { DataService } from "app/_services/data/data.service";
import {
    ChartDataSets,
    ChartOptions,
    ChartPoint,
    ChartType,
    TimeUnit,
} from "chart.js";
import { Color } from "ng2-charts";
import "hammerjs";
import "chartjs-plugin-zoom";

@Component({
    selector: "app-modal-graph-data-sensor",
    templateUrl: "./modal-graph-data-sensor.component.html",
    styleUrls: ["./modal-graph-data-sensor.component.scss"],
})
export class ModalGraphDataSensorComponent implements OnInit {
    widget5SelectedDay = "today";

    ready: boolean = false;
    chartType: ChartType = "line";
    options: ChartOptions;
    dataSet: ChartDataSets[];

    colors: Color[] = [
        {
            borderColor: "#3949ab",
            // backgroundColor: "#3949ab",
            pointBackgroundColor: "#3949ab",
            pointHoverBackgroundColor: "#3949ab",
            pointBorderColor: "#ffffff",
            pointHoverBorderColor: "#ffffff",
        },
    ];

    constructor(
        private dataService: DataService,
        @Inject(MAT_DIALOG_DATA) public data: { sensor: Sensor }
    ) { }

    ngOnInit(): void {
        this.dataService
            .listDataToday(this.data.sensor.id)
            .subscribe((resp) => {
                let dataForDataset: ChartPoint[] = new Array();

                for (let temp of resp.listData) {
                    dataForDataset.push({
                        x: temp.fechaRecolecion,
                        y: temp.dato,
                    });
                }

                this.inicializateGraph(
                    resp.unitMeasure,
                    dataForDataset,
                    "minute"
                );
                this.ready = true;
            });
    }

    graphAllDataSensor() {
        this.dataService.listAllData(this.data.sensor.id).subscribe((resp) => {
            let dataForDataset: ChartPoint[] = new Array();

            for (let temp of resp.listData) {
                dataForDataset.push({
                    x: temp.fechaRecolecion,
                    y: temp.dato,
                });
            }

            this.inicializateGraph(resp.unitMeasure, dataForDataset, "day");
            this.ready = true;
        });
    }

    inicializateGraph(
        labelY: string,
        dataForDataset: ChartPoint[],
        timeUnit: TimeUnit
    ) {
        this.options = {
            responsive: true,
            spanGaps: false,
            legend: {
                display: false,
            },
            maintainAspectRatio: false,
            tooltips: {
                position: "nearest",
                mode: "point",
                intersect: false,
            },
            layout: {
                padding: {
                    left: 24,
                    right: 32,
                },
            },
            elements: {
                point: {
                    radius: 4,
                    borderWidth: 2,
                    hoverRadius: 4,
                    hoverBorderWidth: 2,
                },
            },
            scales: {
                xAxes: [
                    {
                        type: "time",
                        distribution: "linear",
                        gridLines: {
                            display: true,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        time: {
                            unit: timeUnit,
                            displayFormats: {
                                minute: "HH:mm",
                            },
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Fecha Recolección",
                        },
                    },
                ],
                yAxes: [
                    {
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: labelY,
                        },
                        gridLines: {
                            display: true,
                            drawBorder: true,
                            color: "#FF0000",
                        },
                    },
                ],
            },
            plugins: {
                zoom: {
                    pan: {
                        enabled: true,
                        mode: "xy",
                    },
                    zoom: {
                        enabled: true,
                        mode: "xy",
                    },
                },
            },
        };

        this.dataSet = [
            {
                label: "Dato Recolectado",
                data: dataForDataset,
            },
        ];
    }
}
