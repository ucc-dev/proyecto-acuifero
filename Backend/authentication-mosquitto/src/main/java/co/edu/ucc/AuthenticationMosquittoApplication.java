package co.edu.ucc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthenticationMosquittoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationMosquittoApplication.class, args);
	}

}
