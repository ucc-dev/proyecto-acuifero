FROM node:12.7-alpine AS buildAngular
WORKDIR /app/
COPY ./Frontend/ /app/
RUN npm ci
RUN npm run build -- --prod
RUN mv /app/dist/fuse/* /app/dist/


FROM nginx:1.13.8-alpine 
COPY --from=buildAngular /app/dist/ /usr/share/nginx/html
COPY ./Frontend/nginx.conf /etc/nginx/conf.d/default.conf
