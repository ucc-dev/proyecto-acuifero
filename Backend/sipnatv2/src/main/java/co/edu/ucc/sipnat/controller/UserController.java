package co.edu.ucc.sipnat.controller;

import java.util.List;

import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.ucc.sipnat.dto.LogoutDTO;
import co.edu.ucc.sipnat.dto.RefreshTokenDTO;
import co.edu.ucc.sipnat.dto.UserDTO;
import co.edu.ucc.sipnat.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(path = "/signup")
	public ResponseEntity<UserRepresentation> createUser(@RequestBody UserDTO userDTO) {
		return new ResponseEntity<>(userService.createUser(userDTO), HttpStatus.CREATED);
	}

	@PostMapping(path = "/signin")
	public ResponseEntity<AccessTokenResponse> signin(@RequestBody UserDTO userDTO) {
		return ResponseEntity.ok(userService.signin(userDTO));
	}

	@PostMapping(path = "/refresh")
	public ResponseEntity<AccessTokenResponse> refreshToken(@RequestBody RefreshTokenDTO refreshTokenDTO) {
		return ResponseEntity.ok(userService.refreshToken(refreshTokenDTO));
	}

	@PostMapping(path = "/logout")
	public ResponseEntity<?> logout(@RequestBody LogoutDTO logoutDTO) {
		return ResponseEntity.ok(userService.logout(logoutDTO));
	}

	@GetMapping(value = "/unprotected-data")
	public String getName() {
		return "Hello, this api is not protected.";
	}

	@GetMapping(value = "/protected-data")
	public String getEmail() {
		return "Hello, this api is protected.";
	}

	@GetMapping("/list")
	public ResponseEntity<List<UserDTO>> listUsers() {
		return ResponseEntity.ok(userService.listUsers());
	}

	@PatchMapping(path = "/changeStatus")
	public ResponseEntity<Boolean> changeStatus(@RequestBody UserDTO userDTO) {
		return new ResponseEntity<Boolean>(userService.changeStatus(userDTO), HttpStatus.ACCEPTED);
	}

}