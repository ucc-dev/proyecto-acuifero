package co.edu.ucc.sipnat.exception;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import co.edu.ucc.sipnat.dto.ExceptionResponseDTO;

@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

	private Logger logger2 = LogManager.getLogger(ResponseExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> manejarTodasExcepciones(Exception ex, WebRequest request) {
		logger2.fatal(request, ex);
		ExceptionResponseDTO exceptionResponse = new ExceptionResponseDTO(new Date(), ex.getMessage(),
				request.getDescription(true));
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ExceptionUserCreation.class)
	public final ResponseEntity<Object> manejarModeloExcepciones(ExceptionUserCreation ex, WebRequest request) {
		logger2.fatal(request, ex);
		ExceptionResponseDTO exceptionResponse = new ExceptionResponseDTO(new Date(), ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse, ex.getHttpStatus());
	}

	@ExceptionHandler(ExceptionLogicBusiness.class)
	public final ResponseEntity<Object> manejarExcepcionLogicaNegocio(ExceptionLogicBusiness ex, WebRequest request) {
		logger2.fatal(request, ex);
		ExceptionResponseDTO exceptionResponse = new ExceptionResponseDTO(new Date(), ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse, ex.getHttpStatus());
	}
}
