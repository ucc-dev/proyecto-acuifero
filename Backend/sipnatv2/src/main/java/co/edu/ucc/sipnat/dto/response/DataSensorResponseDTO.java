package co.edu.ucc.sipnat.dto.response;

import java.util.List;

import co.edu.ucc.sipnat.dto.DatoDTO;

public class DataSensorResponseDTO {

	private String sensorName;
	private String unitMeasure;
	private List<DatoDTO> listData;

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public String getUnitMeasure() {
		return unitMeasure;
	}

	public void setUnitMeasure(String unitMeasure) {
		this.unitMeasure = unitMeasure;
	}

	public List<DatoDTO> getListData() {
		return listData;
	}

	public void setListData(List<DatoDTO> listData) {
		this.listData = listData;
	}

}
