package co.edu.ucc.sipnat.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.ISensorTypeDAO;
import co.edu.ucc.sipnat.dto.SensorTypeDTO;
import co.edu.ucc.sipnat.entity.TipoSensor;
import co.edu.ucc.sipnat.mapper.SensorTypeMapper;

@Service
public class SensorTypeService {

	@Autowired
	private ISensorTypeDAO sensorTypeDAO;
	@Autowired
	private SensorTypeMapper mapper;

	public TipoSensor createSensorType(SensorTypeDTO sensorTypeDTO) throws Exception {
		try {
			return sensorTypeDAO.save(mapper.dtoToEntity(sensorTypeDTO));
		} catch (IOException e) {
			throw new Exception(e.getMessage());
		}

	}

	public List<SensorTypeDTO> getList() {
		return mapper.listEntityToListDTO(
				sensorTypeDAO.findAll().stream().filter(s -> s.getEstado()).collect(Collectors.toList()));
	}

	public byte[] getIcon(Long id) {
		Optional<TipoSensor> temp = sensorTypeDAO.findById(id);
		if (temp.isPresent()) {
			return temp.get().getLogoDelTipo();
		}

		return null;
	}

	public TipoSensor getById(Long idSensorType) {
		Optional<TipoSensor> optional = sensorTypeDAO.findById(idSensorType);
		return optional.isPresent() ? optional.get() : null;
	}

}
