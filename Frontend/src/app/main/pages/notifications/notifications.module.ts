import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotificationsListComponent } from './notifications-list/notifications-list.component';
import { NotificationDetailComponent } from './notification-detail/notification-detail.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

const routes = [
  {
    path: "notifications",
    component: NotificationsListComponent,
  },
  {
    path: "notification-details/:id",
    component: NotificationDetailComponent,
  },
];

@NgModule({
  declarations: [
    NotificationsListComponent,
    NotificationDetailComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,

    MatButtonModule,
    MatFormFieldModule,

    MatInputModule,
    MatSelectModule,
  ],
  exports: [
    NotificationsListComponent
  ]
})
export class NotificationsModule { }
