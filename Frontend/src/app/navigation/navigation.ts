import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'options',
        title: 'Opciones',
        type: 'group',
        icon: 'apps',
        children: [
            {
                id: 'sensores',
                title: 'Sensores',
                type: 'collapsable',
                icon: 'tune',
                children: [
                    {
                        id: 'tipos-sensor',
                        title: 'Tipo Sensor',
                        type: 'collapsable',
                        children: [
                            {
                                id: 'crear-tipo-sensor',
                                title: 'Crear',
                                type: 'item',
                                url: '/pages/sensor-type'
                            },
                            {
                                id: 'lista-tipo-sensor',
                                title: 'Listar',
                                type: 'item',
                                url: '/pages/sensor-type-list'
                            }
                        ]
                    },
                    {
                        id: 'sensores',
                        title: 'Sensores',
                        type: 'collapsable',
                        children: [
                            {
                                id: 'crear-sensor',
                                title: 'Crear',
                                type: 'item',
                                url: '/pages/sensor'
                            },
                            {
                                id: 'lista-sensores',
                                title: 'Lista',
                                type: 'item',
                                url: '/pages/sensor-list'
                            }
                        ]
                    }
                ]
            },
            {
                id: 'users',
                title: 'Usuarios',
                type: 'item',
                icon: 'account_box',
                url: '/pages/user-list'
            },
            {
                id: 'notifications',
                title: 'Notificaciones',
                type: 'item',
                icon: 'notifications',
                url: '/pages/notifications',
            },
            {
                id: 'dashboards',
                title: 'Tableros',
                type: 'collapsable',
                icon: 'dashboard',
                children: [
                    {
                        id: 'create-dashboard',
                        title: 'Crear',
                        type: 'item',
                        url: '/pages/dashboard-creation'
                    }
                ]
            },
        ]
    },

];
