package co.edu.ucc.sipnat.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class AclMosquitto extends CamposComunesdeEntidad {

	@ManyToOne
	private UserMosquitto userMosquitto;
	@ManyToOne
	private Sensor sensor;
	private String topic;
	private Boolean readPermission;
	private Boolean writePermission;
	private Boolean suscriptionPermission;

	public UserMosquitto getUserMosquitto() {
		return userMosquitto;
	}

	public void setUserMosquitto(UserMosquitto userMosquitto) {
		this.userMosquitto = userMosquitto;
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Boolean getReadPermission() {
		return readPermission;
	}

	public void setReadPermission(Boolean readPermission) {
		this.readPermission = readPermission;
	}

	public Boolean getWritePermission() {
		return writePermission;
	}

	public void setWritePermission(Boolean writePermission) {
		this.writePermission = writePermission;
	}

	public Boolean getSuscriptionPermission() {
		return suscriptionPermission;
	}

	public void setSuscriptionPermission(Boolean suscriptionPermission) {
		this.suscriptionPermission = suscriptionPermission;
	}

}
