package co.edu.ucc.sipnat.dto;

import co.edu.ucc.sipnat.entity.Sensor;
import co.edu.ucc.sipnat.entity.UserMosquitto;

public class ACLMosquittoDTO {

	private UserMosquitto userMosquitto;
	private Sensor sensor;
	private String topic;
	private boolean readPermission;
	private boolean writePermission;
	private boolean suscriptionPermission;

	public UserMosquitto getUserMosquitto() {
		return userMosquitto;
	}

	public void setUserMosquitto(UserMosquitto userMosquitto) {
		this.userMosquitto = userMosquitto;
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public boolean isReadPermission() {
		return readPermission;
	}

	public void setReadPermission(boolean readPermission) {
		this.readPermission = readPermission;
	}

	public boolean isWritePermission() {
		return writePermission;
	}

	public void setWritePermission(boolean writePermission) {
		this.writePermission = writePermission;
	}

	public boolean isSuscriptionPermission() {
		return suscriptionPermission;
	}

	public void setSuscriptionPermission(boolean suscriptionPermission) {
		this.suscriptionPermission = suscriptionPermission;
	}

}
