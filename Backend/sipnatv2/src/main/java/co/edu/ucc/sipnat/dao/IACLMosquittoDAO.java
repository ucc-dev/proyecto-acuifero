package co.edu.ucc.sipnat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.sipnat.entity.AclMosquitto;

@Repository
public interface IACLMosquittoDAO extends JpaRepository<AclMosquitto, Long> {

	@Query(value = "select a from AclMosquitto a where a.estado=true")
	public List<AclMosquitto> findAllTopicByState();

	@Query(value = "select a.topic from acl_mosquitto a where a.sensor_id=:idSensor", nativeQuery = true)
	public String findTopicByIdSensor(@Param("idSensor") Long idSensor);

}
