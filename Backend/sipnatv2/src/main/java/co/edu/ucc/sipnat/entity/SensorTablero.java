package co.edu.ucc.sipnat.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class SensorTablero extends CamposComunesdeEntidad {

	@ManyToOne
	private Tablero tablero;
	@ManyToOne
	private Sensor sensor;

	public Tablero getTablero() {
		return tablero;
	}

	public void setTablero(Tablero tablero) {
		this.tablero = tablero;
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

}
