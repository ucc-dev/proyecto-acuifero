import { ContentObserver } from '@angular/cdk/observers';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { AuthenticationService } from 'app/_services/authentication/authentication.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  animations: fuseAnimations
})
export class UserListComponent implements OnInit, OnDestroy {

  searchInput: FormControl;
  dataSource: any[] = [];
  displayedColumns = [
    "name",
    "email",
    "phoneNumber",
    "role",
    "status",
    "buttons"
  ]

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _fuseSidebarService: FuseSidebarService,
    private authenticationService: AuthenticationService) {
    this.searchInput = new FormControl('');
  }

  ngOnInit(): void {
    // this.searchInput.valueChanges
    //   .pipe(
    //     takeUntil(this._unsubscribeAll),
    //     debounceTime(300),
    //     distinctUntilChanged()
    //   )
    //   .subscribe(searchText => {
    //     // this._contactsService.onSearchTextChanged.next(searchText);
    //   });

    this.authenticationService.listUsers().subscribe(resp => {
      console.log(resp);
      this.dataSource = resp;
    });

  }

  ngOnDestroy(): void {
    // Reset the search
    // this._contactsService.onSearchTextChanged.next('');

    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }

  rolesToString(roles: string[]): string {
    if (roles.length == 1) {
      return this.convertRole(roles[0]);
    } else {
      let otherRole: string;

      for (let item of roles) {
        if (item == "admin_sipnat") {
          return this.convertRole(item);
        } else {
          item = otherRole
        }
      }

      return otherRole;
    }
  }

  convertRole(role: string): string {
    switch (role) {
      case "admin_sipnat":
        return "Administrador"
      case "user_sipnat":
        return "Usuario"
      default:
        break;
    }
  }

  editUser(user: any) {
    console.log(user);
  }

  changeStatusUser(user: any, status: boolean) {
    user.enabled = status;
    this.authenticationService.changeStatus(user).subscribe(resp => {
      console.log(resp);
      this.ngOnInit();
    })
  }

}
