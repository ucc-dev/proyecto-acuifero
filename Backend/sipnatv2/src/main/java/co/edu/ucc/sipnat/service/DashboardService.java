package co.edu.ucc.sipnat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.IDashboardDAO;
import co.edu.ucc.sipnat.dao.ISensorDashboardDAO;
import co.edu.ucc.sipnat.dto.DashboardDTO;
import co.edu.ucc.sipnat.dto.SensorDetailDTO;
import co.edu.ucc.sipnat.dto.response.DataSensorResponseDTO;
import co.edu.ucc.sipnat.entity.Sensor;
import co.edu.ucc.sipnat.entity.SensorTablero;
import co.edu.ucc.sipnat.entity.Tablero;
import co.edu.ucc.sipnat.mapper.DashboardMapper;
import co.edu.ucc.sipnat.mapper.SensorMapper;
import co.edu.ucc.sipnat.util.Constants;

@Service
public class DashboardService {

	@Autowired
	private SensorService sensorService;

	@Autowired
	private IDashboardDAO dashboardDAO;
	@Autowired
	private ISensorDashboardDAO sensorDashboardDAO;
	@Autowired
	private DatoService datoService;

	@Autowired
	private DashboardMapper dashboardMapper;
	@Autowired
	private SensorMapper sensorMapper;

	public Tablero createDashboard(DashboardDTO dashboardDTO) {
		Tablero tablero = dashboardDAO.save(dashboardMapper.dtoToEntity(dashboardDTO));

		for (Long idSensor : dashboardDTO.getIdSensors()) {
			Sensor sensor = sensorService.getById(idSensor);
			if (sensor != null) {
				SensorTablero sensorTablero = new SensorTablero();
				sensorTablero.setSensor(sensor);
				sensorTablero.setTablero(tablero);

				sensorDashboardDAO.save(sensorTablero);
			}
		}

		return tablero;
	}

	public DashboardDTO getDataDashboard(Long idDashboard) {
		Optional<Tablero> optional = dashboardDAO.findById(idDashboard);
		if (optional.isPresent()) {
			return dashboardMapper.entityToDto(optional.get());
		}

		return null;
	}

	public List<SensorDetailDTO> getSensorsDashboard(Long id) {
		List<SensorDetailDTO> sensorDetailDTOs = new ArrayList<>();

		Optional<Tablero> optional = getTablero(id);
		if (optional.isPresent()) {
			List<Sensor> sensors = new ArrayList<>();

			for (SensorTablero sensorTablero : sensorDashboardDAO.findAllByIdTablero(optional.get().getId())) {
				sensors.add(sensorTablero.getSensor());
			}

			sensorDetailDTOs = sensorMapper.entityToDto(sensors);
		}

		return sensorDetailDTOs;
	}

	public List<DataSensorResponseDTO> getDataSensorsDashboard(Long id, String type) {
		List<DataSensorResponseDTO> dataSensorResponseDTOs = new ArrayList<>();

		Optional<Tablero> optional = getTablero(id);
		if (optional.isPresent()) {
			for (SensorTablero sensorTablero : sensorDashboardDAO.findAllByIdTablero(optional.get().getId())) {
				DataSensorResponseDTO sensorResponseDTO = null;

				if (type.equals(Constants.ALL)) {
					sensorResponseDTO = datoService.getAllDataSensor(sensorTablero.getSensor().getId());
				} else {
					sensorResponseDTO = datoService.getDataSensorToday(sensorTablero.getSensor().getId());
				}

				if (sensorResponseDTO != null) {
					sensorResponseDTO.setSensorName(sensorTablero.getSensor().getDescripcion());
					dataSensorResponseDTOs.add(sensorResponseDTO);
				}
			}
		}

		return dataSensorResponseDTOs;
	}

	public List<DashboardDTO> getAllDashboard() {
		return dashboardMapper.entityListToListDTO(dashboardDAO.findAll());
	}

	public Long countSensorsByState(Long id, String state) {
		Long count = 0L;

		Optional<Tablero> optional = getTablero(id);
		if (optional.isPresent()) {
			for (SensorTablero sensorTablero : sensorDashboardDAO.findAllByIdTablero(optional.get().getId())) {
				if (sensorTablero.getSensor().getEstadoDelSensor().equals(state)) {
					count += 1;
				}
			}
		}

		return count;

	}

	private Optional<Tablero> getTablero(Long id) {
		return dashboardDAO.findById(id);
	}

}
