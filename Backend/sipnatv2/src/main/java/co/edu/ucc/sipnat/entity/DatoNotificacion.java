package co.edu.ucc.sipnat.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class DatoNotificacion extends CamposComunesdeEntidad {

	@ManyToOne
	private Notificacion notificacion;
	@ManyToOne
	private Dato dato;

	public Notificacion getNotificacion() {
		return notificacion;
	}

	public void setNotificacion(Notificacion notificacion) {
		this.notificacion = notificacion;
	}

	public Dato getDato() {
		return dato;
	}

	public void setDato(Dato dato) {
		this.dato = dato;
	}

}
