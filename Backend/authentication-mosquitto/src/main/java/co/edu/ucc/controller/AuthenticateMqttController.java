package co.edu.ucc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.ucc.model.AuthorizationAclDTO;
import co.edu.ucc.model.UserDTO;
import co.edu.ucc.service.AuthenticateMqttService;

@RestController
@RequestMapping(value = "/mqtt")
public class AuthenticateMqttController {

	@Autowired
	private AuthenticateMqttService mqttService;

	@PostMapping(path = "/user")
	public ResponseEntity<Boolean> authenticateUser(@RequestBody UserDTO userDTO) {
		return new ResponseEntity<>(mqttService.authenticateUser(userDTO), HttpStatus.OK);
	}

	@PostMapping(path = "/acl")
	public ResponseEntity<Boolean> authorizationTopic(@RequestBody AuthorizationAclDTO aclDTO) {
		return new ResponseEntity<>(mqttService.authorizeUser(aclDTO), HttpStatus.OK);
	}
	
	@PostMapping(path = "/superuser")
	public ResponseEntity<Boolean>  authenticateSuperuser(@RequestBody UserDTO userDTO) {
		return new ResponseEntity<>(mqttService.verifySuperuser(userDTO), HttpStatus.OK);
	}

}
