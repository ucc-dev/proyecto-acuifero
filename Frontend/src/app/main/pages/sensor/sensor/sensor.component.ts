import { Component, OnInit, ViewChild } from "@angular/core";
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from "@angular/forms";
import { MatSelect } from "@angular/material/select";
import { SensorType } from "app/_model/SensorType";
import { SensorTypeService } from "app/_services/sensorType/sensor-type.service";
import { icon, latLng, Marker, tileLayer } from "leaflet";
import { ReplaySubject, Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import * as L from "leaflet";
import { Sensor } from "app/_model/Sensor";
import { SensorService } from "app/_services/sensor/sensor.service";
import swal from "sweetalert2";
@Component({
    selector: "app-sensor",
    templateUrl: "./sensor.component.html",
    styleUrls: ["./sensor.component.scss"],
})
export class SensorComponent implements OnInit {
    form: FormGroup;
    listTypeSensors: SensorType[];
    checkLimits: boolean;

    public bankCtrl: FormControl = new FormControl();
    public typeFilterCtrl: FormControl = new FormControl();
    public filteredTypeSensor: ReplaySubject<SensorType[]> = new ReplaySubject<
        SensorType[]
    >(1);
    @ViewChild("singleSelect", { static: true }) singleSelect: MatSelect;

    map: L.Map;

    iconDefault = icon({
        iconUrl: 'assets/leaflet/marker-icon.png',
        iconRetinaUrl: 'assets/leaflet/marker-icon-2x.png',
        shadowUrl: 'assets/leaflet/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
    });

    options = {
        layers: [
            tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                maxZoom: 20,
                attribution: "...",
            }),
        ],
        zoom: 18,
        center: latLng(46.879966, -121.726909),
        controls: {
            locate: true,
        },
    };

    drawOptions = {
        position: "topleft",
        draw: {
            marker: {
                icon: this.iconDefault,
            },
            polyline: false,
            circle: false,
            rectangle: false,
            circlemarker: false,
            polygon: false,
        },
    };

    constructor(
        private formBuilder: FormBuilder,
        private sensorTypeService: SensorTypeService,
        private sensorService: SensorService
    ) {
        Marker.prototype.options.icon = this.iconDefault;
    }

    protected _onDestroy = new Subject<void>();

    ngOnInit(): void {

        // L.Icon.Default.imagePath = "assests/leaflet/";

        this.inicializateFormGroup();
        this.inicializatelistTypeSensors();
        this.typeFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterBanks();
            });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                this.setGeoLocation.bind(this)
            );
        }
    }

    ngAfterViewInit() {
        this.setInitialValue();
    }

    onMapReady(map: L.Map) {
        this.map = map;
    }

    setGeoLocation(position) {
        this.map.panTo(
            new L.LatLng(position.coords.latitude, position.coords.longitude)
        );
    }

    inicializateFormGroup() {
        this.form = this.formBuilder.group({
            protocol: ["", Validators.required],
            user: [""],
            password: [""],
            type: ["", Validators.required],
            name: ["", Validators.required],
            isLimitsActivate: [false],
            minLimit: [""],
            maxLimit: [""],
            latitude: ["", [Validators.required]],
            longitude: ["", [Validators.required]],
        });
    }

    inicializatelistTypeSensors() {
        this.sensorTypeService.listSensorType().subscribe((resp) => {
            this.listTypeSensors = resp;
            this.bankCtrl.setValue(this.listTypeSensors);
            this.filteredTypeSensor.next(this.listTypeSensors.slice());
        });
    }

    get controls() {
        return this.form.controls;
    }

    protected setInitialValue() {
        this.filteredTypeSensor
            .pipe(take(1), takeUntil(this._onDestroy))
            .subscribe(() => {
                this.singleSelect.compareWith = (
                    a: SensorType,
                    b: SensorType
                ) => a && b && a.id === b.id;
            });
    }

    protected filterBanks() {
        if (!this.listTypeSensors) {
            return;
        }
        // get the search keyword
        let search = this.typeFilterCtrl.value;
        if (!search) {
            this.filteredTypeSensor.next(this.listTypeSensors.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredTypeSensor.next(
            this.listTypeSensors.filter(
                (bank) => bank.name.toLowerCase().indexOf(search) > -1
            )
        );
    }

    onSubmit() {
        if (this.form.valid) {
            swal.fire({
                title: "Creando...",
                text: "Por favor espere",
                allowOutsideClick: false,
                allowEscapeKey: false,
            });

            swal.showLoading();

            let sensor: Sensor = new Sensor();
            sensor.protocol = this.controls.protocol.value;
            sensor.user = this.controls.user.value;
            sensor.password = this.controls.password.value;
            sensor.idSensorType = this.controls.type.value;
            sensor.name = this.controls.name.value;
            if (this.controls.isLimitsActivate.value) {
                sensor.minLimit = this.controls.minLimit.value;
                sensor.maxLimit = this.controls.maxLimit.value;
            }
            sensor.latitude = this.controls.latitude.value;
            sensor.longitude = this.controls.longitude.value;

            this.sensorService.create(sensor).subscribe((resp) => {
                console.log(resp);
                swal.close();
                swal.fire({
                    title: 'Creado con Exito.',
                    text: `Recuerde que ahora puede conectar su sensor con el sistema, al siguiente topic: [${resp.topic}]`,
                    showDenyButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                    icon: 'success'
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.ngOnInit();
                    }
                });
            }, (error) => {
                console.log(error);
                swal.close();
                swal.fire(
                    "ERROR",
                    "Se ha presentado un error, intente nuevamente",
                    "error"
                );
            });
        }
    }

    zonaCreada(event: any) {
        let event1: L.Marker = event.layer;

        this.form.get("latitude").setValue(event1.getLatLng().lat);
        this.form.get("longitude").setValue(event1.getLatLng().lng);

        this.map.addLayer(event1);
    }

    clean() {
        this.ngOnInit();
    }
}
