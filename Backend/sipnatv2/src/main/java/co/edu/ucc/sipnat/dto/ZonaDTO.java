package co.edu.ucc.sipnat.dto;

import java.util.List;

public class ZonaDTO {

	private String nombre;
	private List<DetalleZonaDTO> detalleZonas;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<DetalleZonaDTO> getDetalleZonas() {
		return detalleZonas;
	}

	public void setDetalleZonas(List<DetalleZonaDTO> detalleZonas) {
		this.detalleZonas = detalleZonas;
	}

}
