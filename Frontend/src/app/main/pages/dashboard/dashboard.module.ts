import { NgModule } from '@angular/core';
import { DashboardCreateComponent } from './dashboard-create/dashboard-create.component';
import { RouterModule } from '@angular/router';
import { MatStepperModule } from '@angular/material/stepper';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatMenuModule } from '@angular/material/menu';
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FuseWidgetModule } from '@fuse/components';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';


const routes = [
  {
    path: "dashboard-creation",
    component: DashboardCreateComponent,
  },
  {
    path: "dashboard/:id",
    component: DashboardComponent
  }
];

@NgModule({
  declarations: [DashboardCreateComponent, DashboardComponent],
  imports: [
    RouterModule.forChild(routes),
    MatStepperModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FuseSharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatMenuModule,
    ChartsModule,
    NgxChartsModule,

    FuseWidgetModule,

    LeafletModule,
    LeafletDrawModule
  ],
  exports: [DashboardCreateComponent]
})
export class DashboardModule { }
