package co.edu.ucc.sipnat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dao.IUserMosquittoDAO;
import co.edu.ucc.sipnat.dto.UserMosquittoDTO;
import co.edu.ucc.sipnat.entity.UserMosquitto;
import co.edu.ucc.sipnat.mapper.UserMosquittoMapper;
import co.edu.ucc.sipnat.util.Utils;

@Service
@Order(Ordered.HIGHEST_PRECEDENCE)
public class UserMosquittoService {

	@Value("${mqtt.key_secret}")
	private String clientSecret;

	@Autowired
	private IUserMosquittoDAO userMosquittoDAO;
	@Autowired
	private UserMosquittoMapper userMosquittoMapper;

	public UserMosquitto create(UserMosquittoDTO dto) {
		return create(userMosquittoMapper.dtoToEntity(dto));
	}

	public UserMosquitto create(UserMosquitto userMosquitto) {
		return userMosquittoDAO.save(userMosquitto);
	}

	public UserMosquitto find(String username, String password) {
		String passwordEncode = Utils.calculateSecretHash(username, password, clientSecret);
		return userMosquittoDAO.findByUsernameAndPassword(username, passwordEncode);
	}

}
