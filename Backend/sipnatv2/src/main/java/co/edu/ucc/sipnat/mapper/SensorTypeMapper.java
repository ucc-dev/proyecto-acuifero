package co.edu.ucc.sipnat.mapper;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import co.edu.ucc.sipnat.dto.SensorTypeDTO;
import co.edu.ucc.sipnat.entity.TipoSensor;

@Service
public class SensorTypeMapper {

	public TipoSensor dtoToEntity(SensorTypeDTO sensorTypeDTO) throws IOException {
		TipoSensor tipoSensor = new TipoSensor();

		tipoSensor.setNombre(sensorTypeDTO.getName());
		tipoSensor.setMagnitud(sensorTypeDTO.getMagnitude());
		tipoSensor.setUnidadDeMedida(sensorTypeDTO.getUnitMeasure());
		tipoSensor.setLogoDelTipo(sensorTypeDTO.getFile().getBytes());

		return tipoSensor;

	}

	public List<SensorTypeDTO> listEntityToListDTO(List<TipoSensor> listTipoSensors) {
		return listTipoSensors.stream().map(entity -> {
			SensorTypeDTO dto = new SensorTypeDTO();
			dto.setId(entity.getId());
			dto.setName(entity.getNombre());
			dto.setMagnitude(entity.getMagnitud());
			dto.setUnitMeasure(entity.getUnidadDeMedida());
			dto.setIcon(entity.getLogoDelTipo());
			return dto;
		}).collect(Collectors.toList());

	}
}
