export class SensorType {
  id: number;
  name: string;
  magnitude: string;
  unitMeasure: string;
  icon: File;

  constructor() { }
}
